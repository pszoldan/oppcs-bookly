var videoClient;
var activeRoom;
var identity;
var roomName;
var participant_count = 0;
var waitingForConnect;

const localMedia = new Twilio.Video.LocalMedia();

// Check for WebRTC
if (!navigator.webkitGetUserMedia && !navigator.mozGetUserMedia) {
  alert('WebRTC is not available in your browser.');
}

jQuery(document).ready(function() {
    video_log("Videó token kérése a szerverről");
    jQuery.ajax({
        url : twilio_backend.ajax_url,
        type : 'post',
        data : {
            action : 'twilio_generate_new_token'
        },
        success : function( raw_response ) {
            video_log("Lejátszó indítása");
            try {
                response =  JSON.parse(raw_response);
                if (response.success)
                {
                    identity = response.identity;
                    roomName = jQuery('#twilio-room-id').attr('value');
                    video_log("Várakozás a kamera és mikrofon megosztására");
                    collectStreams(response.token);
                }
                else
                    video_log(response.error);
            }
            catch (error) {
                video_log(error.message);
            }
        },
        error : function( response ) {
            video_log("Hiba a lejátszó indítása közben: " + response);
        }
    });
});

// When we are about to transition away from this page, disconnect
// from the room, if joined.
window.addEventListener('beforeunload', leaveRoomIfJoined);

function collectStreams(token)
{
  // Try to collect camera + microphone
  Twilio.Video.getUserMedia().then(mediaStream => {
    console.log('Streams added');
    localMedia.addStream(mediaStream);
    connectRoom(token);
  // camera is not available
  }, function(error) {
    video_log('Nem sikerült a kamerát megosztani, próbálkozás csak mikrofonnal');
    collectMicrophoneOnly(token);
  });
};

function collectMicrophoneOnly(token)
{
  localMedia.addMicrophone().then(() => {
    console.log('Microphone added');
    connectRoom(token);
  }, function(error) {
      video_log('Nem sikerült a mikrofont megosztani: ' + error.message);
  });
}

function connectRoom(token)
{
  videoClient = new Twilio.Video.Client(token);

  videoClient.connect({
    to: roomName,
    localMedia: localMedia
  }).then(roomJoined,
    function(error) {
      video_log('Sikertelen csatlakozás a szobához: ' + error.message);
  });
}

function create_new_video_container(id)
{
    var container = document.createElement('div');
    container.className = 'video_container';
    container.id = id;
    container.addEventListener("click", change_main_video, false);
    document.getElementById('participants').appendChild(container);

    return '#' + id;
};

function move_video(container_id, to)
{
    var video_container = jQuery('#' + container_id);

    if (to == '#main-video' && waitingForConnect && container_id == 'remoteparticipant')
    {
        jQuery('#main-video').addClass('oppcs_no_video_msg');
    }
    else
    {
        jQuery('#main-video').removeClass('oppcs_no_video_msg');
    }

    if (container_id == 'localparticipant')
    {
        activeRoom.localParticipant.media.detach();
        jQuery(to).append(video_container);
        activeRoom.localParticipant.media.attach('#' + container_id);
    }
    else
    {
        participant_id = jQuery(video_container).data('participant_id');
        if (activeRoom.participants.get(participant_id) == null)
        {
            jQuery(to).append(video_container);
            return;
        }

        activeRoom.participants.get(participant_id).media.detach();
        jQuery(to).append(video_container);
        activeRoom.participants.get(participant_id).media.attach('#' + container_id);
    }
}

function change_main_video(event)
{
    var video_container = jQuery(event.target);
    if (!jQuery(event.target).hasClass('video_container'))
    {
        video_container = jQuery(event.target).parent('.video_container');
    }
    var participant_id = jQuery(video_container).attr('id');

    var main_video = jQuery('#main-video .video_container');
    var main_video_id = jQuery(main_video).attr('id');

    // nothing to do if it is already in the main video
    if (main_video_id == participant_id)
        return;

    if (main_video.length > 0)
    {
        move_video(main_video_id, '#participants');
    }

    move_video(participant_id, '#main-video');
};

function cleanup_unused_containers()
{
    jQuery('.video_container:empty').remove();
}

function showWaitingScreen()
{
  jQuery('#remoteparticipant').html('<p>Várakozás a másik fél csatlakozására</p>').addClass('waiting_for_remote_party');
}

function hideWaitingScreen()
{
  jQuery('#remoteparticipant').html('').removeClass('waiting_for_remote_party');
  jQuery('#main-video').removeClass('oppcs_no_video_msg');
}


// Successfully connected!
function roomJoined(room) {
  console.log('Joining to the room');
  activeRoom = room;

  var container_id = create_new_video_container('localparticipant');
  room.localParticipant.media.attach(container_id);

  var container_id = create_new_video_container('remoteparticipant');
  showWaitingScreen();

  jQuery('#main-video').html('');
  move_video('remoteparticipant', '#main-video');
  waitingForConnect = true;

  room.participants.forEach(function(participant, key) {
    if (waitingForConnect)
    {
        waitingForConnect = false;
        container_id = '#remoteparticipant';
        hideWaitingScreen();
    }
    else
    {
        container_id = create_new_video_container(key);
    }
    jQuery(container_id).data('participant_id', key);
    participant.media.attach(container_id);
  });

  // When a participant joins, draw their video on screen
  room.on('participantConnected', function (participant) {
    var key = participant.sid;
    if (waitingForConnect)
    {
        waitingForConnect = false;
        hideWaitingScreen();
        container_id = '#remoteparticipant';
    }
    else
    {
        container_id = create_new_video_container(key);
    }
    jQuery(container_id).data('participant_id', key);
    participant.media.attach(container_id);
  });

  // Someone left the meeting
  room.on('participantDisconnected', function (participant) {
    participant.media.detach();
    if (activeRoom.participants.size == 0)
    {
        waitingForConnect = true;
        showWaitingScreen();
    }
    cleanup_unused_containers();
  });

  // When we are disconnected, stop capturing local video
  // Also remove media for all remote participants
  room.on('disconnected', function () {
    room.localParticipant.media.detach();
    room.participants.forEach(function(participant) {
      participant.media.detach();
    });
    activeRoom = null;
    cleanup_unused_containers();
  });
}

function video_log(message){
    jQuery('#main-video').addClass('oppcs_no_video_msg');
    jQuery('#video-log').html(message);
}

function leaveRoomIfJoined() {
  if (activeRoom) {
    activeRoom.disconnect();
  }
}
