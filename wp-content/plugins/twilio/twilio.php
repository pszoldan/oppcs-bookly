<?php

/**
 * Plugin Name: Twilio WebRTC plugin
 * Plugin URI: http://easyitme.com
 * Description: Integrating Twilio WebRTC into via shortcode
 * Version: 1.0.0
 * Author: Akos Adam Medgyes
 * Author URI: http://easyitme.com
 * License: strict
 *
 */

define('TWILIO_RESOURCE_VERSION', '1.0');

foreach(scandir(__DIR__) as $f)
{
    $spl = new SplFileInfo(__DIR__ . '/' . $f);
    if ($spl->isFile() && 'php' == $spl->getExtension())
    {
        require_once($spl->getPathname());
    }
}

add_shortcode('twilio_video_feed', 'twilio_video_feed');

add_action('wp_enqueue_scripts', 'twilio_register_script');
function twilio_register_script()
{
    wp_register_script("twilio-video", plugins_url('twilio-video.min.js', __FILE__), array(), TWILIO_RESOURCE_VERSION);

    wp_register_script("twilio", plugins_url('twilio.js', __FILE__),
        array('jquery', 'wp-ajax-response'),
        TWILIO_RESOURCE_VERSION
    );

    wp_localize_script('twilio', 'twilio_backend', array(
        'ajax_url' => admin_url('admin-ajax.php')
    ));
}

add_action('wp_enqueue_scripts', 'twilio_register_style');
function twilio_register_style()
{
    wp_register_style("twilio", plugins_url('twilio.css', __FILE__), array(), TWILIO_RESOURCE_VERSION);
}

/*
 * Expected case sensitive arguments of the shortcode
 *
 * @param id    string The ID of the room, required by Twilio
 * @param title string The name of the room, used only for informing the user
 */
function twilio_video_feed($raw_args = [])
{
    $args = shortcode_Atts(array(
        'id' => 'Test id',
        'title' => 'Test title'
    ), $raw_args);

    wp_enqueue_script("twilio-video");
    wp_enqueue_script("twilio");
    wp_enqueue_style("twilio");

    debug_logger_new_entry( array( 
    		'error_level' => 10,
    		'plugin' => 'Twilio',
    		'comment' => $args['id']) );

    echo <<<EOF
    <h5 class="twilio-title">${args['title']}</h5>
    <input id="twilio-room-id" type="hidden" value="${args['id']}"/>
    <div id="main-video" class="oppcs_video_your_next_appointment oppcs_no_video_msg">
        <p id="video-log"></p>
    </div>
    <div id="participants"></div>
EOF;
}
