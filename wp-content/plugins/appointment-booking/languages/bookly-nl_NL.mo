��    �     �  �  �      8)     9)  %   ;)  &   a)     �)     �)     �)     �)  
   �)     �)  |  �)  @   f+     �+     �+     �+     �+     �+  d   �+     U,     d,     �,  
   �,     �,     �,     �,  	   �,  F   �,      -     (-     <-     @-     H-     U-     ]-     o-     }-  	   �-  *   �-     �-     �-     �-  
   �-     �-     �-     �-     .     .      .     '.  �   ,.     �.     �.     �.  �   �.  C   �/     �/  @  �/     !1     *1     61     P1     W1     s1     |1     �1     �1     �1     �1     �1     �1  %   �1  	   �1     2     2     2      2  	   (2     22     :2     G2  �   T2     �2  	   �2     3  '   	3     13     93     P3     W3  +   _3  ;   �3     �3    �3     �4     �4     5     5     !5     *5  	   85     B5     G5     K5  �   T5    <6    H7  �   K8    09  �   8:  	   8;  &   B;     i;  !   ~;  !   �;      �;     �;     �;     �;     <     <  	   6<  	   @<     J<     R<     [<  %   h<     �<  2   �<     �<  	   �<     �<     �<     �<     �<     =     =     &=     <=     E=     U=     ]=  (   x=     �=     �=     �=  t   �=     =>     K>  :   a>     �>     �>     �>     �>  S   �>  M   G?     �?     �?     �?     �?     �?     �?     �?  <   �?  	   )@  I   3@     }@     �@     �@  	   �@     �@  m   �@  i   !A     �A     �A  "   �A     �A  �   �A  �   �B  �   C  �   �C  9   �D  �   �D  �   �E  �   ]F  7   %G     ]G     mG     }G     �G     �G     �G     �G  �   �G  �   �H  >   PI  �   �I  �   rJ  �   K  �   �K  �   }L  J   �L  {   JM  t   �M     ;N  :   BN  L   }N  A   �N  ,   O  �   9O  6   �O     P     #P     @P     TP     mP     tP  X   �P     �P     �P     Q     Q     ,Q     ;Q     HQ  
   TQ     _Q     pQ  
   �Q     �Q     �Q     �Q     �Q  m   �Q     #R  =   +R  )   iR     �R     �R     �R     �R     �R     �R     �R     �R     �R     S  
   S  
   S     &S  $   )S     NS     dS     uS  -   �S  '   �S     �S  +   �S     T      1T     RT  �   WT     "U     (U  0   9U  2   jU  A   �U  ;   �U  6   V     RV  $   `V     �V     �V     �V     �V     �V     �V     �V     �V     �V     	W     W     W     ;W     CW     LW     SW     hW     nW     �W     �W  #   �W     �W  �   �W  e   �X    �X     �Y     
Z     #Z  '   ;Z  "   cZ  $   �Z  #   �Z     �Z  *   �Z     [     ,[     E[     _[     n[  
   t[     [     �[  	   �[     �[     �[  	   �[     �[     �[     �[     �[     �[     \     #\     ,\     9\     L\     \\     i\     u\     }\     �\     �\     �\     �\     �\     �\     �\     �\  E  ]     Q^     q^     ~^     �^     �^  
   �^     �^  &   �^     �^  �   �^     �_     �_     �_     �_  P   �_  �   `     �`  .   �`     �`     a     'a     @a     Oa     ]a     ja     va     ~a     �a     �a     �a     �a  �   �a  �   ;b    �b     �c     �c     d     d     +d     Gd  	   bd  I   ld  h   �d     e     %e     2e  
   @e     Ke     he     oe  G   we     �e     �e     �e  	   �e  
   �e  %   f  `   +f  �   �f  2   /g  6   bg  X   �g  0   �g  /   #h  '   Sh  6   {h  M   �h  ,    i  -   -i  +   [i  
   �i  ,   �i     �i     �i  
   �i     �i     �i     j     
j  ;   j  Z   Ij  k   �j     k     k     k     /k  7   4k     lk     zk     �k     �k  a  �k     m     m     m  �   )m  '   �m     n     #n     +n  	   0n     :n  "  Mn  Y   po     �o  	   �o  �   �o  �   mp  j   q  �   mq  �   �q  !   �r  $   �r     $s     As  �  Ns  .   �t  "   u     8u  	   Wu     au     yu     �u  $   �u  =   �u     �u     v     (v     <v     Mv  
   ]v     hv     xv  
   �v     �v     �v     �v     �v     �v     �v     �v     w     w     %w     6w     Cw     ]w     sw     �w     �w     �w  J   �w     �w     x    !x     1{  %   3{  %   Y{     {     �{     �{     �{     �{     �{  �  �{  >   �}     �}     �}     �}     �}     ~  l   ~     �~     �~  "   �~     �~     �~     �~          $  U   3     �     �     �     �     �     �     �     �     �      �  +   �     =�     D�     P�     ^�     g�     o�     x�  	   ��     ��     ��     ��  �   ��     P�     \�     t�  �   ��  ?   .�     n�  x  w�     ��     ��     �  	   !�     +�  
   C�     N�     V�     r�  	   ��     ��  
   ��     ��  .   Ƅ  	   ��     ��     �     �     �  	   !�     +�     8�     E�  �   R�  	   �  	   ��     �  )   �     2�     7�     J�     V�  1   e�  O   ��     �  $  ��     �     -�     4�     F�     \�  	   b�     l�     t�     z�     ~�    ��    ��    ��    ċ    ǌ    �     �  ,   �     =�  &   P�  '   w�  $   ��     ď     Џ     ��     �     �     �  
   -�     8�     I�     W�  $   c�     ��  <   ��     Ԑ  
   ڐ     �     �     �     �     �     %�     5�  
   S�     ^�     o�     |�  .   ��     Ǒ     ӑ     �  z   �     n�  &   ��  :   ��     �  !   �     
�     )�  [   H�  R   ��     ��     �     �      �     (�     C�     J�  ?   [�  	   ��  E   ��     �     ��     �     �     %�  Z   .�  k   ��     ��     �  ;   �     X�  �   ]�  g  @�  �   ��  �   ��  \   ��  g  �  �   P�  �   A�  \   3�     ��     ��  	   ��     ��     ў     �      �  �   &�  �   �  O   ��    ��  �   �  �   ��  �   Z�  �   4�  O   Ƥ  {   �  o   ��  
   �  =   �  I   K�  D   ��  9   ڦ  �   �  7   ̧     �  !   �     4�     H�     ^�     g�  `   ��     �     ��     �      �     5�     D�     U�     e�     u�  +   ��     ��     ��     ��  	   ɩ  	   ө  h   ݩ     F�  I   N�  %   ��     ��     Ī     ɪ     ڪ     �     ��      
�     +�     8�     I�     R�     a�     p�  +   t�     ��     ��     ի  :   �  ,   �     K�  9   h�     ��  *   ��  
   �  �   ��     խ     ޭ  /   ��  .   (�  4   W�  /   ��  3   ��  	   �  )   ��     $�     4�     I�     M�     ]�     c�     l�     y�     ��  
   ��  
   ��  #   ��  
   ݯ  
   �     �     ��     �      �     =�     U�  '   Z�     ��  �   ��  S   B�  *  ��     ��     ݲ     �  "   �  %   )�     O�     o�     ��     ��     ��     ҳ     �     ��     �  
   �     �  
   !�     ,�     :�     J�     V�     _�     n�  	   ��     ��     ��     ��     ��     ɴ     մ     �     ��     	�     �     !�     4�     M�     `�  	   �     ��  
   ��     ��     ��  V  ��     �     0�     =�     E�     X�  
   d�     o�  0   {�     ��  �   ķ     o�     ��     ��     ��  J   ��  �   
�     ѹ  K   �     4�     C�     _�     x�     ��     ��     ��  	   ��  	   ź     Ϻ     ֺ     �     ��  �   �  �   ��  R  H�     ��     ��  #   ��     �     ��     �     3�  <   @�  �   }�  
   ,�  
   7�     B�     N�     Z�     w�  	   ~�  F   ��  "   Ͽ     �     �     �  	    �  ,   *�  d   W�  �   ��  ;   k�  ?   ��  Y   ��  *   A�  /   l�  )   ��  =   ��  U   �  -   Z�  -   ��  8   ��  
   ��  /   ��      *�     K�     P�     X�     i�     o�     v�  =   z�  g   ��  b    �     ��     ��     ��     ��  6   ��     ��     ��  
   �     �  �  /�     ��  	   ��     ��  �   ��  4   ��  "   ��     �     �  	   "�     ,�  h  ?�  f   ��     �     �  �   �  �   ��  s   @�    ��  �   ��  "   }�  9   ��      ��     ��  �  �     ��     ��      ��     �      �     5�  '   =�  4   e�  R   ��     ��     �      �     3�     M�     e�     t�     ��     ��     ��     ��     ��     ��     ��     ��     �     (�     F�     Z�     k�  (   y�     ��     ��     ��     ��     ��  Z   ��     Z�     j�     �   l  V   �   A   �       S         �  �   �          �   O   q  �       �  �        I   �   �  �  ~          �  .      �   �  �   �  ;   �  �        �   �  �   �   �       �   =   �   �      E  f  �   �   m  �  �   �  	           �   |  �   )       �       �  �  .          F      M  l   �   �      �   �   �       Y  "   C   �          �  )  4   X   �   �      8  `      �  �  d        �   �       n   �  w   %   _        N   5  �   u   Q  �  �  Y   ;  #     �   �      e  ~      '     L  0  j          �   h   �   w        �  �   �  *                         j   �     �       �   �   ]      �      P   �      k  �  #              �  E   8   �   	      �              �   a  b      �   �   Q   }      {   e   �      1     *      �  2      �          �       �   k   9   �             �  �      x   }         �   �   D          �   9  q       ?      �  x        �               g          T  K           �   o  �       H       �       
         �  2   1           ^   d   �      �   +  �        r   �       B  ]       �      �   [          �   3   y   B   �  (       �   /              L       �          &  �  �   r      G   �   �  �              m             ,   �      -  p  v      �  =  �       U   �  
  6   �          T       v   p   �     �      �  5   �   �   �   +   �   `       �   :      o          W       '   �   �  O  �   $  �   H         �          u  V  �          (      i   �  �         �          �   R        3  �   �   >           �    G                   �   �  \   �   �      �  I  �   �  �   �  �   %  �     �  �  �  ?       �         �  g   R   �       A  �   �       J   W        �  �  �       �  �   �                       �  @       �  �  c         �  �  M   Z         �   t   U  �                  �  &   \  �  �   {  �       D      <  �  s  �          �      �  �       P  ^  K  y                  -           �          �   �   >              �           :       �           �  _   �       �     �   �   h  �  �   �   �  �   6    �  /     �   �   �  a   �               @  !        �   7  �   �  0   �  |      !    �  �  �  �   b   F  �          �    �   �    7   �   S           [   <   c  �  �  J  �  �   �           �  �  s   �   �       C  4                  �  �       �   z  ,  �  t  �   f   �  �  i  �   �   X      n  �  �  z   Z   N  �  �  "  �   �  �     �           �  $   �   �   �   �    
 "%s" is too long (%d characters max). "%s" is too short (%d characters min). %d h %d min -- Search customers -- -- Select a service -- 2 way sync 2Checkout Standard Checkout <b>Important!</b> Please be aware that if your copy of Bookly was not downloaded from Codecanyon (the only channel of Bookly distribution), you may put your website under significant risk - it is very likely that it contains a malicious code, a trojan or a backdoor. Please consider buying a licensed copy of Bookly <a href="http://booking-wp-plugin.com" target="_blank">here</a>. <b>Leave this field empty</b> to work with the default calendar. API Login ID API Password API Signature API Transaction Key API Username Accept <a href="javascript:void(0)" data-toggle="modal" data-target="#ab-tos">Terms & Conditions</a> Account Number Add Bookly appointments list Add Bookly booking form Add Coupon Add Service Add Services Add Staff Members Add money Add services and assign them to the staff members you created earlier. Address Administrator phone All All Day All Services All day All payment types All providers All services All staff Allow staff members to edit their profiles Amount Another code Any Appearance Apply Appointment Appointment Date Appointments Are you sure? Avatar Back Below you can find a list of available time slots for [[SERVICE_NAME]] by [[STAFF_NAME]].
Click on a time slot to proceed with booking. Booking Time Booking cancellation Booking product Bookly offers a simple solution for making appointments. With our plugin you will be able to easily manage your availability time and handle the flow of your clients. Bookly: You do not have sufficient permissions to access this page. Business hours By default Bookly pushes new appointments and any further changes to Google Calendar. If you enable this option then Bookly will fetch events from Google Calendar and remove corresponding time slots before displaying the second step of the booking form (this may lead to a delay when users click Next at the first step). Calendar Calendar ID Calendar ID is not valid. Cancel Cancel appointment page URL Capacity Captcha Card Security Code Cart item data Category Change password Checkbox Checkbox Group Click on the underlined text to edit. Client ID Client secret Code Codes Columns Comma (,) Company Company logo Company name Configure what information should be places in the title of Google Calendar event. Available codes are [[SERVICE_NAME]], [[STAFF_NAME]] and [[CLIENT_NAMES]]. Connect Connected Cost Could not save appointment in database. Country Country out of service Coupon Coupons Create WordPress user account for customers Create a product in WooCommerce that can be placed in cart. Create customer Create your project's OAuth 2.0 credentials by clicking <b>Create new Client ID</b>, selecting <b>Web application</b>, and providing the information needed to create the credentials. For <b>AUTHORIZED REDIRECT URIS</b> enter the <b>Redirect URI</b> found below on this page. Credit Card Number Currency Custom Fields Custom Range Customer Customer Name Customers Date Day Days off Dear [[CLIENT_NAME]].

Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].

Thank you and we look forward to seeing you again soon.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].

This is confirmation that you have booked [[SERVICE_NAME]].

We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].

We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].

Thank you for choosing our company.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
Thank you for choosing [[COMPANY_NAME]]. We hope you were satisfied with your [[SERVICE_NAME]].
Thank you and we look forward to seeing you again soon.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
This is confirmation that you have booked [[SERVICE_NAME]].
We are waiting you at [[COMPANY_ADDRESS]] on [[APPOINTMENT_DATE]] at [[APPOINTMENT_TIME]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Dear [[CLIENT_NAME]].
We would like to remind you that you have booked [[SERVICE_NAME]] tomorrow on [[APPOINTMENT_TIME]]. We are waiting you at [[COMPANY_ADDRESS]].
Thank you for choosing our company.
[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Deduction Deduction should be a positive number. Default country code Default value for category select Default value for employee select Default value for service select Delete Delete break Delete current photo Delete customers Delete this staff member Delimiter Delivered Details Disabled Discount (%) Discount should be between 0 and 100. Dismiss Display available time slots in client's time zone Done Drop Down Duration Edit Edit appointment Edit booking details Email Email Notifications Email already in use. Employee Empty password. Enabled End time must not be empty End time must not be equal to start time Enter a URL Enter a label Enter a name Enter a phone number in international format. E.g. for the United States a valid phone number would be +17327572923. Enter a value Enter code from email Enter this URL as a redirect URI in the Developers Console Error Error adding the break interval Error connecting to server. Error sending email. Evening notification with the next day agenda to staff member (requires cron setup) Evening reminder to customer about next day appointment (requires cron setup) Expiration Date Expired Export to CSV Failed Failed to send SMS. Filter Final step URL Finally provide the necessary information in the form below. Finish by Follow-up message in the same day after appointment (requires cron setup) Forgot password Form ID error. From Full name General Go to Staff Members, select a staff member and click on "Connect" which is located at the bottom of the page. Go to the <a href="https://console.developers.google.com/" target="_blank">Google Developers Console</a>. Google Calendar Google Calendar integration Guess country by user's IP address HTML HTML allows formatting, colors, fonts, positioning, etc. With Text you must use Text mode of rich-text editors below. On some servers only text emails are sent successfully. Hello.

An account was created for you at [[SITE_ADDRESS]]

Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hello.

The following booking has been cancelled.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.

You have new booking.

Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.

Your agenda for tomorrow is:

[[NEXT_DAY_AGENDA]] Hello.
An account was created for you at [[SITE_ADDRESS]]
Your user details:
user: [[NEW_USERNAME]]
password: [[NEW_PASSWORD]]

Thanks. Hello.
The following booking has been cancelled.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.
You have new booking.
Service: [[SERVICE_NAME]]
Date: [[APPOINTMENT_DATE]]
Time: [[APPOINTMENT_TIME]]
Client name: [[CLIENT_NAME]]
Client phone: [[CLIENT_PHONE]]
Client email: [[CLIENT_EMAIL]] Hello.
Your agenda for tomorrow is:
[[NEXT_DAY_AGENDA]] Hide this block Hide this field Holidays I will pay locally I will pay now with Credit Card I will pay now with PayPal I'm available on or after If <b>Publishable Key</b> is provided then Bookly will use <a href="https://stripe.com/docs/stripe.js" target="_blank">Stripe.js</a><br/>for collecting credit card details. If email or SMS notifications are enabled and you want the customer or the staff member to be notified about this appointment after saving, tick this checkbox before clicking Save. If needed, edit item data which will be displayed in the cart. If there is a lot of events in Google Calendar sometimes this leads to a lack of memory in PHP when Bookly tries to fetch all events. You can limit the number of fetched events here. This only works when 2 way sync is enabled. If this option is enabled then all staff members who are associated with WordPress users will be able to edit their own profiles, services, schedule and days off. If this option is enabled then the email address of the customer is used as a sender email address for notifications sent to staff members and administrators. If this setting is enabled then Bookly will be creating WordPress user accounts for all new customers. If the user is logged in then the new customer will be associated with the existing user account. If this staff member requires separate login to access personal calendar, a regular WP user needs to be created for this purpose. If you are not redirected automatically, follow the <a href="%s">link</a>. If you do not see your country in the list please contact us at <a href="mailto:support@ladela.com">support@ladela.com</a>. If you will leave this field blank, this staff member will not be able to access personal calendar using WP backend. Import In <b>Approved URL</b> enter the URL of your booking page. In <b>Checkout Options</b> of your 2Checkout account do the following steps: In <b>Direct Return</b> select <b>Header Redirect (Your URL)</b>. In the form below enable WooCommerce option. In the sidebar on the left, expand <b>APIs & auth</b>. Next, click <b>APIs</b>. In the list of APIs, make sure the status is <b>ON</b> for the Google Calendar API. In the sidebar on the left, select <b>Credentials</b>. Incorrect code Incorrect email or password. Incorrect password. Incorrect recovery code. Insert Insert Appointment Booking Form Insert a URL of a page that is shown to clients after they have cancelled their booking. Instructions Invalid date or time Invalid email Invalid email. Invalid number Last 30 Days Last 7 Days Last Month Last appointment Limit number of fetched events Loading... Local Log In Log out Login Look for the <b>Client ID</b> and <b>Client secret</b> in the table associated with each of your credentials. Message Method to include Bookly JavaScript and CSS files on the page Minimum time requirement prior to booking Month Name New Category New Customer New Staff Member New appointment New booking information New customer New password Next Next Month Next month No No appointments for selected period. No appointments found No coupons found No customers. No payments for selected period and criteria. No services found. Please add services. No staff selected No time is available for selected criteria. No, delete just customers No, update just here in services Note Note that once you have enabled WooCommerce option in Bookly the built-in payment methods will no longer work. All your customers will be redirected to WooCommerce cart instead of standard payment step. Notes Notes (optional) Notification settings were updated successfully. Notification to customer about appointment details Notification to customer about their WordPress user login details Notification to staff member about appointment cancellation Notification to staff member about appointment details Notifications Number of days available for booking Number of persons Number of times used OFF Old password Option Order Out of credit Padding time (before and after) Page Redirection Participants Password Passwords must be the same. Payment Payments Period Personal Information Phone Phone field default country Phone number is empty. Photo Please accept terms and conditions. Please add your staff members. Please be aware that a value in this field is required in the frontend. If you choose to hide this field, please be sure to select a default value for it Please configure Google Calendar <a href="?page=ab-settings&type=_google_calendar">settings</a> first Please do not forget to specify your purchase code in Bookly <a href="admin.php?page=ab-settings">settings</a>. Upon providing the code you will have access to free updates of Bookly. Updates may contain functionality improvements and important security fixes. Please enter old password. Please select a customer Please select a service Please select at least one appointment. Please select at least one coupon. Please select at least one customer. Please select at least one service. Please select service:  Please tell us how you would like to pay:  Please tell us your email Please tell us your name Please tell us your phone Previous month Price Price list Profile Provider Providers Publishable Key Purchase Code Purchases Queued Quick search customer Radio Button Radio Button Group Recovery code expired. Redirect URI Register Registration Remember my choice Remove customer Remove field Remove item Reorder Repeat every year Repeat new password Repeat password Reply directly to customers Required Required field Reset SMS Details SMS Notifications SMS Notifications (or "Bookly SMS") is a service for notifying your customers via text messages which are sent to mobile phones.<br/>It is necessary to register in order to start using this service.<br/>After registration you will need to configure notification messages and top up your balance in order to start sending SMS. SMS has been sent successfully. Sandbox Mode Save Save Member Schedule Secret Key Secret Word Select a project, or create a new one. Select category Select default country for the phone field in the 'Details' step of booking. You can also let Bookly determine the country based on the IP address of the client. Select file Select from WP users Select product Select service Select the product that you created at step 1 in the drop down list of products. Select the time interval that will be used in frontend and backend, e.g. in calendar, second step of the booking process, while indicating the working hours, etc. Selected / maximum Selected period doesn't match service duration Semicolon (;) Send copy to administrators Send email notifications Send emails as Send test SMS Sender email Sender name Sending Sent Service Service paid locally Services Session error. Set a URL of a page that the user will be forwarded to after successful booking. If disabled then the default step 5 is displayed. Set a minimum amount of time before the chosen appointment (for example, require the customer to book at least 1 hour before the appointment time). Set padding time before and/or after an appointment. For example, if you require 15 minutes to prepare for the next appointment then you should set "padding before" to 15 min. If there is an appointment from 8:00 to 9:00 then the next available time slot will be 9:15 rather than 9:00. Settings Settings saved. Show blocked timeslots Show calendar Show each day in one column Show form progress tracker Signed up Sorry, the time slot %date_time% for %service% has been already occupied. Specify the number of days that should be available for booking at step 2 starting from the current day. Staff Staff Member Staff Members Start from Start time must not be empty Status Subject Synchronize the data of the staff member bookings with Google Calendar. Template for event title Terms & Conditions Text Text Area Text Field Thank you for purchasing our product. Thank you! Your booking is complete. An email with details of your booking has been sent to you. The Calendar ID can be found by clicking on "Calendar settings" next to the calendar you wish to display. The Calendar ID is then shown beside "Calendar Address". The client ID obtained from the Developers Console The client secret obtained from the Developers Console The maximum number of customers allowed to book the service for the certain time period. The number of customers should be not more than  The price for the service is [[SERVICE_PRICE]]. The requested interval is not available The selected period is occupied by another appointment The selected time is not available anymore. Please, choose another time slot. The start time must be less than the end one The start time must be less than the end time The value is taken from client’s browser. This Month This coupon code is invalid or has been used This email is already in use Time Time range Time slot length Title Titles To To find your client ID and client secret, do the following: To send scheduled notifications please execute the following script hourly with your cron: To start using Bookly, you need to follow these steps which are the minimum requirements to get it running! Today Total Total appointments Type URL for cancel appointment link (to use inside <a> tag) Uncategorized Undelivered Untitled Update service setting Upon providing the purchase code you will have access to free updates of Bookly. Updates may contain functionality improvements and important security fixes. For more information on where to find your purchase code see this <a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-can-I-find-my-Purchase-Code-" target="_blank">page</a>. Usage limit User User not found. User with "Administrator" role will have access to calendars and settings of all staff members, user with some other role will have access only to personal calendar and settings. Visible to non-logged in customers only We are not working on this day Website Week Week days Welcome to Bookly! With "Enqueue" method the JavaScript and CSS files of Bookly will be included on all pages of your website. This method should work with all themes. With "Print" method the files will be included only on the pages which contain Bookly booking form. This method may not work with all themes. WooCommerce cart is not set up. Follow the <a href="%s">link</a> to correct this problem. Yes Yesterday You are about to change a service setting which is also configured separately for each staff member. Do you want to update it in staff settings too? You are about to delete customers which may have WordPress accounts associated to them. Do you want to delete those accounts too (if there are any)? You may import list of clients in CSV format. The file needs to have three columns: Name, Phone and Email. You need to install and activate WooCommerce plugin before using the options below.<br/><br/>Once the plugin is activated do the following steps: You selected a booking for [[SERVICE_NAME]] by [[STAFF_NAME]] at [[SERVICE_TIME]] on [[SERVICE_DATE]]. The price for the service is [[SERVICE_PRICE]].
Please provide your details in the form below to proceed with booking. Your agenda for [[TOMORROW_DATE]] Your appointment at [[COMPANY_NAME]] Your appointment information Your balance Your clients must have their phone numbers in international format in order to receive text messages. However you can specify a default country code that will be used as a prefix for all phone numbers that do not start with "+" or "00". E.g. if you enter "1" as the default country code and a client enters their phone as "(600) 555-2222" the resulting phone number to send the SMS to will be "+1600555222". Your payment has been accepted for processing. Your payment has been interrupted. Your visit to [[COMPANY_NAME]] add break address of your company breaks: cancel appointment link combined values of all custom fields combined values of all custom fields (formatted in 2 columns) customer new password customer new username date of appointment date of next day date of service disconnect email of client email of staff login form name of category name of client name of service name of staff name of your company number of persons phone of client phone of staff photo of staff price of service site address staff agenda for next day this web-site address time of appointment time of service to total price of booking total price of booking (service price multiplied by the number of persons) your company logo your company phone Project-Id-Version: Bookly v7.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-09-22 16:52+0300
PO-Revision-Date: 2015-09-29 14:21+0300
Last-Translator: 
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1,2t;_e:1,2t;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3
X-Poedit-Basepath: .
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: ..
 
 "%s" is te lang (maximaal %d tekens). "%s" is te kort (minimaal %d tekens). %d uur %d min -- Zoek klanten -- -- Selecteer een dienst -- 2-wegs synchronisatie 2Checkout Standaard Checkout <b>Belangrijk!</b> Wanneer uw exemplaar van Bookly niet gedownload is van CodeCanyon (het enige distributiekanaal van Bookly), u uw website groot gevaar kan lopen - het is zeer waarschijnlijk dat het kwaadaardige code bevat, een Trojaans paard of een achterdeur in uw site. Neem geen risico en schaf u officiëlel exemplaar van Bookly <a href="http://booking-wp-plugin.com" target="_blank">hier</a> aan. <b>Laat dit veld leeg</b> om de standaard agenda te gebruiken. API Login ID API wachtwoord API handtekening API Transaction Key API gebruikersnaam Accepteer de <a href="javascript:void(0)" data-toggle="modal" data-target="#ab-tos">Algemene Voorwaarden</a> Account Number Bookly afsprakenlijst toevoegen Bookly boekingsformulier toevoegen Kortingsbon toevoegen Dienst toevoegen Diensten Toevoegen Medewerkers Toevoegen Geld toevoegen Voeg diensten toe, en wijs deze toe aan de medewerkers die u eerder heeft aangemaakt. Adres Beheerder telefoon Allemaal Hele dag Alle diensten Hele dag Alle betalingssoorten Alle medewerkers Alle diensten Alle medewerkers Laat medewerkers hun eigen profiel bewerken Bedrag Andere code Geen voorkeur Weergave Opslaan Afspraak Datum afspraak Afspraken Weet u het zeker? Profielfoto Vorige Hieronder vind u een lijst van beschikbare tijdsblokken voor [[SERVICE_NAME]] door [[STAFF_NAME]]. 
Klik op een tijdsblok om door te gaan met reserveren. Reservering Reservering geannuleerd Reserveringsproduct Bookly biedt een eenvoudige oplossing voor het maken van afspraken. Onze plugin laat u uw beschikbare tijd eenvoudig beheren, en beheert de afspraken van uw klanten. Bookly: U heeft onvoldoende rechten om deze pagina te bekijken. Werkuren Standaard stuurt Bookly nieuwe afspraken en alle verdere wijzigingen naar Google Agenda. Wanneer u deze optie inschakelt zal Bookly gebeurtenissen uit Google Agenda halen, en overeenkomende tijdsblokken verwijderen voordat de tweede stap van het reserveringsformulier wordt getoond (dit kan leiden tot een vertraging wanneer gebruikers op Volgende klikken bij de eerste stap). Kalender Kalender ID Kalender ID is niet geldig. Annuleren URL annulering afspraak Capaciteit Captcha Creditcard beveiligingscode Winkelwagen productgegevens Categorie Verander wachtwoord Keuzevakje Keuzevak Groep Klik op de onderstreepte tekst om te wijzigen. Client ID Client secret Code Codes Kolommen Komma (,) Bedrijfsnaam Bedrijfslogo Bedrijfsnaam Stel in welke informatie in de titel van Google Agenda gebeurtenissen moet staan. Beschikbare codes: [[SERVICE_NAME]], [[STAFF_NAME]] en [[CLIENT_NAMES]]. Verbinden Verbonden Kosten Kon afspraak niet opslaan in de database. Land Land buiten dienst Kortingsbon Kortingsbonnen Maak een WordPress gebruikersaccount voor klanten Maak een product in WooCommerce dat in het winkelwagentje geplaatst kan worden. Klant aanmaken Maak de OAuth 2.0 credentials van uw project. Klik op <b>Create new Client ID</b>, selecteer van <b>Web application</b>, en vul de informatie in die nodig is om de credentials aan te maken. Bij <b>AUTHORIZED REDIRECT URIS</b> voer u de <b>Redirect URI</b>in die onderaan op deze pagina staat. Creditcard nummer Valuta Aangepaste velden Aangepast datumbereik Klant Klantnaam Klanten Datum Dag Vrije dagen Hallo [[CLIENT_NAME]].

Bedankt voor het reserveren van een afspraak bij [[COMPANY_NAME]]. Wij hopen dat u tevreden was met de gereserveerde dienst [[SERVICE_NAME]].

Bedankt en hopelijk snel tot ziens.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Hallo [[CLIENT_NAME]].

Hierbij bevestigen wij uw [[SERVICE_NAME]] afspraak.

Wij verwachten u op [[APPOINTMENT_DATE]] om [[APPOINTMENT_TIME]].

Wij zijn gevestigd op:

[[COMPANY_ADDRESS]]

Bedankt voor uw vertrouwen in ons.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Hallo [[CLIENT_NAME]].

Ik wil u er graag aan herinneren dat u morgen om [[APPOINTMENT_TIME]] een [[SERVICE_NAME]] afspraak hebt met [[STAFF_NAME]].

Wij zijn gevestigd op:

[[COMPANY_ADDRESS]]

Bedankt voor uw vertrouwen in ons.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Hallo [[CLIENT_NAME]].

Bedankt voor het reserveren van een afspraak bij [[COMPANY_NAME]]. Wij hopen dat u tevreden was met de gereserveerde dienst [[SERVICE_NAME]].

Bedankt en hopelijk snel tot ziens.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Hallo [[CLIENT_NAME]].

Dit is een bevestiging voor uw [[SERVICE_NAME]] afspraak.

We verwachten u op [[APPOINTMENT_DATE]] om [[APPOINTMENT_TIME]].

Wij zijn gevestigd op:

[[COMPANY_ADDRESS]]

Bedankt voor uw vertrouwen in ons.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Hallo [[CLIENT_NAME]].

Ik wil u er graag aan herinneren dat u morgen om [[APPOINTMENT_TIME]] een [[SERVICE_NAME]] afspraak hebt met [[STAFF_NAME]].

Wij zijn gevestigd op:

[[COMPANY_ADDRESS]]

Bedankt voor uw vertrouwen in ons.

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Korting (€) Korting in € moet een positief getal zijn. Standaard landcode Standaardwaarde selectieveld categorie Standaardwaarde selectieveld medewerker Standaardwaarde selectieveld dienst  Verwijderen Verwijder pauze Wis huidige foto Verwijder klanten Verwijder deze medewerker Scheidingsteken Afgeleverd Persoonsgegevens Uitgeschakeld Korting (%) Korting moet tussen 0 en 100 liggen. Ongedaan maken Toon de beschikbare tijdsblokken in de tijdzone van de klant Klaar Keuzelijst Duur Bewerken Wijzig afspraak Bewerk reserveringsdetails E-mail Email meldingen Emailadres wordt al gebruikt. Medewerker Leeg wachtwoord. Ingeschakeld Eindtijd mag niet leeg zijn Eindtijd mag niet gelijk zijn aan de begintijd Voer URL in Naam toevoegen Voer een naam in Voer een telefoonnummer in internationaal formaat in. In Nederland is een geldig telefoonnummer bijvoorbeeld +31202225555. Voer een waarde in Voer de per email toegestuurde code in Voer deze URL in als Redirect URL in de Developers Console Fout Fout bij toevoegen pauze-interval Fout bij verbinden met server. Fout bij verzenden emailadres. Melding in de avond aan medewerkers met de agenda voor de volgende dag (vereist cron setup) Herinnering aan de klant de avond voor de dag van de afspraak (vereist cron setup) Verloopdatum Verlopen Exporteer naar CSV Mislukt Kon de SMS niet versturen. Filter URL laatste stap Vul daarna de benodigde informatie in onderstaand formulier in. Eindig om Follow-up bericht op dezelfde dag na de afspraak (vereist cron setup) Wachtwoord vergeten Formulier ID fout. Van Volledige naam Algemeen Ga naar Medewerkers, selecteer een medewerker, en klik op "Verbinden", onderaan de pagina. Ga naar de <a href="https://console.developers.google.com/" target="_blank">Google Developers Console</a> . Google Agenda Google Agenda integratie Raad het land aan de hand van het IP-adres van de gebruiker HTML HTML staat opmaak, kleuren, lettertypen, positionering, enz. toe. Met platte tekst moet je de tekst modus van de rich-text editors hieronder gebruiken. Op sommige servers worden alleen platte tekst emails met succes verstuurd. Hallo [[CLIENT_NAME]],

Er is een gebruikersnaam voor u gemaakt op [[SITE_ADDRESS]]

Uw gebruikersgegevens zijn:

Gebruikersnaam: [[NEW_USERNAME]]
Wachtwoord: [[NEW_PASSWORD]]

U kunt deze gebruiken om uw afspraken te bekijken en eventueel te annuleren op de reserveringspagina.

Met vriendelijke groet,

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Hallo [[STAFF_NAME]],

De volgende reservering is geannuleerd.

Dienst: [[SERVICE_NAME]]
Datum: [[APPOINTMENT_DATE]]
Tijd: [[APPOINTMENT_TIME]]

Klantnaam: [[CLIENT_NAME]]
Klant telefoonnummer: [[CLIENT_PHONE]]
Klant email: [[CLIENT_EMAIL]] Hallo [[STAFF_NAME]],

Je hebt een nieuwe reservering binnengekregen:

Dienst: [[SERVICE_NAME]]
Datum: [[APPOINTMENT_DATE]]
Tijd: [[APPOINTMENT_TIME]]

Klantnaam: [[CLIENT_NAME]]
Klant telefoon: [[CLIENT_PHONE]]
Klant email: [[CLIENT_EMAIL]] Hallo [[STAFF_NAME]],

Jouw agenda voor morgen ziet er als volgt uit: 

 [[NEXT_DAY_AGENDA]] Hallo [[CLIENT_NAME]],

Er is een gebruikersnaam voor u gemaakt op [[SITE_ADDRESS]]

Uw gebruikersgegevens zijn:

Gebruikersnaam: [[NEW_USERNAME]]
Wachtwoord: [[NEW_PASSWORD]]

U kunt deze gebruiken om uw afspraken te bekijken en eventueel te annuleren op de reserveringspagina.

Met vriendelijke groet,

[[COMPANY_NAME]]
[[COMPANY_PHONE]]
[[COMPANY_WEBSITE]] Hallo [[STAFF_NAME]],

De volgende reservering is geannuleerd.

Dienst: [[SERVICE_NAME]]
Datum: [[APPOINTMENT_DATE]]
Tijd: [[APPOINTMENT_TIME]]

Klantnaam: [[CLIENT_NAME]]
Klant telefoonnummer: [[CLIENT_PHONE]]
Klant email: [[CLIENT_EMAIL]] Hallo [[STAFF_NAME]],

Je hebt een nieuwe reservering binnengekregen:

Dienst: [[SERVICE_NAME]]
Datum: [[APPOINTMENT_DATE]]
Tijd: [[APPOINTMENT_TIME]]

Klantnaam: [[CLIENT_NAME]]
Klant telefoon: [[CLIENT_PHONE]]
Klant email: [[CLIENT_EMAIL]] Hallo [[STAFF_NAME]],

Jouw agenda voor morgen ziet er als volgt uit: 

 [[NEXT_DAY_AGENDA]] Verberg dit blok Verberg dit veld Vakanties Ik betaal ter plekke Ik betaal nu met creditcard Ik betaal nu met Paypal Ik ben zelf beschikbaar op of na Wanneer <b>Publishable Key</b> is ingevoerd, zal Bookly gebruik maken <br>van <a href="https://stripe.com/docs/stripe.js" target="_blank">Stripe.js</a> om de creditcardgegevens op te halen. Als email of SMS-meldingen zijn ingeschakeld en u wilt dat de klant of de medewerker worden geïnformeerd over deze afspraak na het opslaan, zet dan een vinkje in het vakje voordat u op Opslaan klikt. Bewerk, indien nodig, de gegevens die in het winkelwagentje worden weergegeven. Wanneer er veel gebeurtenissen in Google Agenda staan, kan dit soms leiden tot geheugenproblemen in PHP wanneer Bookly probeert alle gebeurtenissen op te halen. U kunt het aantal op te halen gebeurtenissen hier beperken. Dit werkt alleen wanneer 2 way synchronisatie is ingeschakeld. Wanneer deze optie is ingeschakeld kunnen alle medewerkers die ook een WordPress account hebben hun eigen profiel, diensten, planning, en vrije dagen bewerken. Wanneer deze optie is geactiveerd, wordt het emailadres van de klant gebruikt als het afzender emailadres voor meldingen gestuurd naar medewerkers en beheerder. Wanneer deze optie is ingeschakeld, zal Bookly een WordPress gebruikersaccount maken voor alle nieuwe klanten. Is de gebruiker al ingelogd, dan zal de nieuwe klant worden gekoppeld aan het bestaande gebruikersaccount. Wanneer deze medewerker een aparte login nodig heeft voor haar/zijn persoonlijke agenda, moet hiervoor een gewone WP-gebruiker aangemaakt worden. Volg, wanneer u niet automatisch wordt doorgestuurd,  de <a href="%s">link</a>. Staat uw land niet in de lijst, neem dan contact met ons op via <a href="mailto:support@ladela.com">support@ladela.com</a>. Wanneer u dit veld leeg laat, kan de medewerker zijn/haar persoonlijke agenda niet benaderen via de WP backend. Importeren In <b>Approved URL</b>, typ  de URL van uw reserveringspagina Doe het volgende in de <b>Checkout Options</b> van uw 2Checkout rekening: In <b>Direct Return</b>, selecteer <b>Header Redirect (Your URL)</b> Schakel in onderstaand formulier de WooCommerce optie in. In de linker navigatiekolom, klik op  <b>APIs & auth.</b>. Klik vervolgens <b>APIs.</b>. Zorg ervoor dat in de lijst van APIs de status voor de Google Calendar API op <b>ON</b> staat. In de linker navigatiekolom, klik op <b>Credentials</b> Onjuiste code Onjuist emailadres of wachtwoord. Onjuist wachtwoord. Onjuiste herstelcode. Invoegen Reserveringsformulier toevoegen De URL van de pagina die wordt getoond aan klanten nadat zij hun reservering hebben geannuleerd. Instructies Ongeldige datum of tijd Ongeldig emailadres Ongeldig emailadres. Ongeldig getal Laatste 30 dagen Laatste 7 dagen Afgelopen maand Laatste afspraak Beperk het aantal opgehaalde gebeurtenissen Laden... Cash Inloggen Uitloggen Loginnaam Zoek naar de <b>Client ID</b> en <b>Client secret</b> in de tabel die hoort bij  elk van uw credentials. Bericht Methode om de Bookly JavaScript en CSS-bestanden in de pagina op te nemen Minimum tijd voorafgaand aan afspraak Maand Naam Nieuwe categorie Nieuwe klant Nieuwe medewerker  Nieuwe afspraak Nieuwe reservering binnengekomen Nieuwe klant Nieuw wachtwoord Volgende Volgende maand Volgende maand Nee Geen afspraken in de geselecteerde periode. Geen afspraken gevonden Geen kortingsbonnen gevonden Geen klanten. Geen betalingen voor de geselecteerde periode en criteria. Geen diensten gevonden. Voeg een dienst toe. Geen medewerker geselecteerd Er is geen tijdsblok vrij voor de geselecteerde criteria. Nee, verwijder alleen klanten Nee, wijzig het alleen hier in de diensten Belangrijk Let op: wanneer de WooCommerce optie is ingeschakeld werken de in Bookly ingebouwde betaalmethoden niet meer. Al uw klanten worden doorgestuurd naar de WooCommerce winkelwagen in plaats van naar de standaard betalingsstap. Notities Aantekeningen (optioneel) Meldingsinstellingen zijn succesvol opgeslagen. Melding aan de klant met alle afspraakgegevens Melding aan de klant met hun WordPress inloggegevens Melding aan medewerker over annulering afspraak Melding aan de medewerker met alle afspraakgegevens Meldingen Aantal dagen beschikbaar voor reservering Aantal personen Aantal keer gebruikt UIT Oude wachtwoord Optie Volgorde Geen krediet Extra tijd (vóór en na) Paginaomleiding Deelnemers Wachtwoord Wachtwoorden moeten hetzelfde zijn. Betalingen Betalingen Periode Persoonlijke informatie Telefoon Standaard land voor telefoonveld Telefoonnummer is leeg. Foto Accepteer alstublieft onze voorwaarden. Voeg uw medewerkers toe. Het ius verplicht om in het frontend een waarde in dit veld te hebben.. Kiest u ervoor dit veld te verbergen, dan moet u een standaard waarde voor dit veld selecteren Configureer eerst de <a href="?page=ab-settings">instellingen</a> van Google Agenda Vergeet niet uw aankoopcode in te voeren in Bookly onder <a href="admin.php?page=ab-settings">instellingen</a>. Het invoeren van de code geeft u toegang tot gratis updates van Bookly. Updates kunnen verbeteringen in de functionaliteit en oplossingen voor belangrijke beveiligingsproblemen bevatten. Vul het oude wachtwoord in. Selecteer een klant Selecteer een dienst Selecteer minimaal één afspraak. Selecteer minimaal één kortingsbon. Selecteer minimaal één klant. Selecteer minimaal één dienst Selecteer een dienst: Hoe wenst u te betalen: Vul uw emailadres in Vul uw naam in Vul uw telefoonnummer in Vorige maand Tarief Prijslijst Profiel Medewerker Medewerker(s) Publishable Key Aankoopcode Aankopen In de wachtrij Snel klant zoeken Radioknop Radioknop Groep Herstelcode verlopen. Redirect URL Registreren Registreren Onthoud mijn keuze Verwijder klant Veld verwijderen Verwijderen Herschikken Herhaal ieder jaar Herhaal nieuw wachtwoord Herhaal wachtwoord Reageer direct naar de klanten Verplicht Verplicht veld Herstellen SMS details SMS meldingen SMS Meldingen (of "Bookly SMS") is een dienst voor het vertsuren van meldingen aan uw klanten via sms-berichten naar mobiele telefoons.<br/><br/>Het is noodzakelijk om u te registreren om van deze dienst gebruik te kunnen maken.<br/><br/>Na registratie moet u de meldingen instellen en uw saldo aanvullen om SMS meldingen te kunnen versturen. SMS is succesvol verstuurd. Sandbox Mode Opslaan Medewerker opslaan Werkrooster Secret Key Secret Word Selecteer een project of maak een nieuw project. Selecteer een categorie Selecteer het standaard land voor het telefoonveld in de 'Details' van de reservering. U kunt Bookly het land ook laten bepalen aan de hand van het IP-adres van de klant. Selecteer bestand Kies uit WP-gebruikers Selecteer product Selecteer een dienst Selecteer het product dat u heeft gemaakt bij stap 1 in de productenlijst. Selecteer de tijdsinterval die moet worden gebruikt in de frontend en de backend, bijvoorbeeld in de kalender, in de tweede stap van het reserveringsproces, bij de vermelding van de werktijden, enz. Geselecteerd / maximum Geselecteerde periode komt niet overeen met de standaard duur van de dienst Punt-komma (;) Stuur kopie naar beheerders Verstuur email meldingen E-mails versturen als Stuur test SMS E-mail afzender Naam afzender Verzenden Verzonden Dienst Dienst ter plaatse betalen Diensten Sessie-fout. De URL van de pagina waar de gebruiker naar toegezonden wordt na een succesvolle reservering te hebben gemaakt. Indien uitgeschakeld wordt de standaard stap 5 weergegeven. Stel een minimum tijdsduur in vóór de gekozen afspraak (bijvoorbeeld eisen dat de klant minimaal 1 uur vóór de tijd van de afspraak reserveert). Stel de extra tijd vóór en/of na een afspraak in. Bijvoorbeeld, wanneer je 15 minuten nodig hebt om je voor te bereiden op de volgende afspraak, zet dan de extra tijd dan moet u de extra tijd vóór op 15 min zetten. Is er een afspraak van 8:00 tot 09:00, dan bent u volgens de kalender weer beschikbaar vanaf 09:15 in plaats van 09:00. Instellingen Instellingen opgeslagen. Geblokkeerde tijdsblokken weergeven Kalender weergeven Toon elke dag in één kolom Laat voortgang formulier zien Ingeschreven Sorry, het tijdsblok %date_time% voor %service% is al bezet. Geef het aantal dagen op dat de klant maximaal vooruit kan boeken tijdens stap 2 vanaf de huidige dag. Bijvoorbeeld maximaal 30 dagen vanaf nu beschikbaar voor reserveringen. Medewerker Medewerker Medewerkers Begin vanaf Begintijd mag niet leeg zijn Status Onderwerp Synchroniseer de afspraakgegevens van de medewerker met Google Agenda. Template voor het gebeurtenistitel Algemene Voorwaarden platte tekst Tekstgebied Tekstveld Bedankt dat u ons product heeft aangeschaft. Bedankt! Uw reservering is voltooid. Een email met de details van uw reservering is naar u gestuurd. U kunt de Agenda ID vinden door op het pijltje naast de weer te geven agenda te klikken en "Agenda-instellingen" te selecteren. De Agenda ID staat achter "Adres voor agenda". Het client ID dat u heeft gevonden in de Developers Console Het client secret dat u heeft gevonden in de Developers Console Het maximum aantal klanten die deze activiteit in dezelfde tijdsperiode mogen reserveren. Het aantal klanten mag niet meer zijn dan  De prijs voor deze dienst is [[SERVICE_PRICE]]. De gevraagde interval is niet beschikbaar De geselecteerde periode wordt bezet door een andere afspraak De geselecteerde tijd is niet meer beschikbaar. Kies alstublieft een ander tijdsblok. De begintijd moet eerder zijn dan de eindtijd De begintijd moet eerder zijn dan de eindtijd Deze waarde wordt opgehaald uit de browser van de klant. Deze maand Deze kortingscode is ongeldig of is al gebruikt Dit emailadres wordt al gebruikt Tijd Periode Lengte tijdsblok Titel Titels Tot Om uw klant ID en klantgeheim te vinden, doet u het volgende: Om meldingen te kunnen inplannen, moet u het volgende script ieder uur door uw cron te laten uitvoeren: Volg deze stappen om te beginnen met Bookly, zodat u het reserveringssysteem goed draaiend krijgt. Vandaag Totaal Totaal aantal afspraken Soort URL voor annuleringslink (te gebruiken binnen <a> tag) Geen categorie Niet afgeleverd Geen titel Dienst instellingen bijwerken De aankoopcode geeft u  toegang tot gratis updates van Bookly. Updates kunnen verbeteringen in de functionaliteit en oplossingen voor belangrijke beveiligingsproblemen bevatten.<br><br>Voor meer informatie over het vinden van uw aankoop code kunt u de volgende <a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-can-I-find-my-Purchase-Code-" target="_blank">pagina</a> doorlezen. Gebruikslimiet Gebruiker Gebruiker niet gevonden. Een gebruiker met een "Beheerder" rol heeft toegang tot de agenda en instellingen van alle medewerkers, gebruikers met een andere rol hebben alleen toegang tot hun persoonlijke agenda en instellingen. Alleen zichtbaar voor klanten die niet zijn ingelogd Wij zijn niet aanwezig op deze dag Website Week Weekdagen Welkom bij Bookly! De "Enqueue" methode zorgt ervoor dat de JavaScript en CSS-bestanden van Bookly in alle pagina's van uw website worden opgenomen. Deze methode zou moeten werken met alle thema's. De "Print" methode zorgt ervoor dat de bestanden worden alleen opgenomen op de pagina's die het Bookly boekingsformulier bevatten. Deze methode werkt mogelijk niet met alle thema's. WooCommerce winkelwagen is niet ingesteld. Volg de <a href="%s">link</a> om dit probleem te verhelpen. Ja Gisteren U gaat een dienst-instelling wijzigen die ook apart is ingesteld voor elke medewerker. Wilt u deze wijziging ook in de instellingen van de medewerker doorvoeren? U verwijdert klanten die waarschijnlijk ook een WordPress account hebben. Wilt u deze accounts ook verwijderen (als deze er zijn)? U kunt een klantenlijst in CSV formaat importeren. Het bestand moet drie kolommen hebben: naam, telefoon, en email. U moet eerst de  gratis <a href="https://nl.wordpress.org/plugins/woocommerce/" target="blank">WooCommerce plugin</a> installeren en activeren voor u onderstaande opties kunt gebruiken. <br/><br/>Volg - wanneer de plugin is geactiveerd - de volgende stappen: U heeft een reservering voor [[SERVICE_NAME]] door [[STAFF_NAME]] op [[SERVICE_TIME]] op [[SERVICE_DATE]] geselecteerd.
Vul uw gegevens in het formulier hieronder in om door te gaan met reserveren. Jouw agenda voor [[TOMORROW_DATE]] Herinnering: uw afspraak voor morgen bij [[COMPANY_NAME]] Alle informatie over uw afspraak Uw saldo Uw klanten moeten hun telefoonnummers in internationaal formaat invoeren om sms-berichten te kunnen ontvangen. U kunt ook een standaard land code instellen die gebruikt zal worden als prefix van alle telefoonnummers die niet beginnen met "+" of "00". Bv. voert u "31" in als standaard landcode en een klant typt haar telefoonnummer in als "(600) 555-2222", dan wordt het telefoonnummer waar de SMS heen wordt gestuurd: "+31600555222". Uw betaling wordt verwerkt. Uw betaling is onderbroken. Uw afspraak bij [[COMPANY_NAME]] pauze toevoegen adres van uw bedrijf pauzes: link voor het annuleren van de afspraak combinatie van de waarden van alle aangepaste velden combinatie van de waarden van alle aangepaste velden (geformatteerd in 2 kolommen) nieuw wachtwoord klant nieuwe gebruikersnaam klant datum van afspraak datum van de volgende dag datum van de activiteit niet verbonden email adres van klant email van medewerker inlogformulier categorienaam naam van klant naam van dienst naam van medewerker uw bedrijfsnaam aantal personen telefoonnummer van klant telefoonnummer van medewerker foto van medewerker prijs van dienst website adres medewerker's agenda voor de volgende dag dit website adres tijd van afspraak begintijd van dienst tot totale prijs van de reservering totale prijs van de reservering (prijs van dienst vermenigvuldigd met het aantal personen) uw bedrijfslogo telefoonnummer van uw bedrijf 