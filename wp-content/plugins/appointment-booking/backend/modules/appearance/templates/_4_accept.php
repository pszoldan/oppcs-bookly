<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="ab-booking-form">
    <!-- Progress Tracker-->
    <?php $step = 4; include '_progress_tracker.php'; ?>

    <h3>Client Section</h3>
    
    <div class="ab-row-fluid">
    	<h5>This text always appears for the client:</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_info_fourth_step' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-text-info-fourth" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_info_fourth_step' ) ) ?></span>
    </div>

    <div class="ab-row-fluid">
    	<h5>This text appears for the client when acceptance is pending:</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_step_accept_pending' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-text-info-fourth-pending" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_step_accept_pending' ) ) ?></span>
    </div>

    <div class="ab-row-fluid">
    	<h5>This text appears for the client when the appointment is accepted:</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_step_accept_accepted' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-text-info-fourth-accepted" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_step_accept_accepted' ) ) ?></span>
    </div>

    <div class="ab-row-fluid">
    	<h5>This text appears for the client when the appointment is rejected:</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_step_accept_rejected' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-text-info-fourth-rejected" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_step_accept_rejected' ) ) ?></span>
    </div>

    <div class="ab-row-fluid last-row ab-nav-steps ab-clear">
        <button class="ab-left ab-cancel-appointment ab-btn ladda-button">
            <span><?php _e( 'Cancel Appointment', 'bookly' ) ?></span>
        </button>
        <button class="ab-right ab-to-fifth-step ab-btn ladda-button">
            <span><?php _e( 'Next', 'bookly' ) ?></span>
        </button>
    </div>
    
    <h3><br />Provider Section</h3>
    
    <div class="ab-row-fluid">
    	<h5>This text always appears for the provider:</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_staff_accept_info' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-appearance-text-staff-accept-info" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_staff_accept_info' ) ) ?></span>
    </div>
    
    <div class="ab-row-fluid">
    	<h5>This text appears for the provider when acceptance is pending:</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_staff_accept_pending' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-appearance-text-staff-accept-pending" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_staff_accept_pending' ) ) ?></span>
    </div>
        
    <div class="ab-row-fluid">
    	<h5>This text appears for the provider when the appointment is already accepted:</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_staff_accept_accepted' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-appearance-text-staff-accept-accepted" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_staff_accept_accepted' ) ) ?></span>
    </div>    

    <div class="ab-row-fluid">
    	<h5>This text appears for the provider when the appointment is booked (paid):</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_staff_accept_booked' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-appearance-text-staff-accept-booked" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_staff_accept_booked' ) ) ?></span>
    </div>    

    <div class="ab-row-fluid">
    	<h5>This text appears for the provider when the appointment is already rejected:</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_staff_accept_rejected' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-appearance-text-staff-accept-rejected" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_staff_accept_rejected' ) ) ?></span>
    </div>    
    
    <div class="ab-row-fluid">
    	<h5>This text appears for the provider when the appointment is not found (e.g., cancelled by client in the meantime):</h5>
        <span data-inputclass="input-xxlarge" data-notes="<?php
        	echo esc_attr( $this->render( '_codes', array( 'step' => 4, 'login' => false ), false ) )
        	 ?>" data-placement="bottom" data-default="<?php
        	 echo esc_attr( get_option( 'ab_appearance_text_staff_accept_not_found' ) ) 
        	 ?>" class="ab-text-info-fourth-preview ab-row-fluid ab_editable" id="ab-appearance-text-staff-accept-not-found" data-type="textarea"><?php 
        	 echo esc_html( get_option( 'ab_appearance_text_staff_accept_not_found' ) ) ?></span>
    </div>    
    
    <div class="ab-row-fluid last-row ab-nav-steps ab-clear">
        <button class="ab-left ab-reject-appointment ab-btn ladda-button">
            <span><?php _e( 'Reject', 'bookly' ) ?></span>
        </button>
        <button class="ab-right ab-accept-appointment ab-btn ladda-button">
            <span><?php _e( 'Accept', 'bookly' ) ?></span>
        </button>
    </div>
    
</div>
