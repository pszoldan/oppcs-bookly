<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
    /** @var AB_UserBookingData $userData */
    echo $progress_tracker;

    if ($info_text): ?>
<div class="ab-row-fluid">
    <div class="ab-desc"><?php echo $info_text; ?></div>
</div><?php
	endif;
?><div class="ab-row-fluid">
    <div class="ab-desc"><?php echo $appointment_status_message; ?></div>
</div><?php 
if($is_current_user_staff):

	if ('pending' == $appointment_status):
?><div class="ab-row-fluid ab-nav-steps ab-clear ab-staff-accept">
		<button class="ab-left ab-reject-appointment ab-btn ladda-button" data-style="zoom-in" style="margin-right: 10px;" data-spinner-size="40">
    	    <span class="ladda-label"><?php echo _o( 'Reject', 'bookly', 'button' ) ?></span>
    	</button>
	    <button class="ab-right ab-accept-appointment ab-btn ladda-button" data-style="zoom-in" data-spinner-size="40">
	        <span class="ladda-label"><?php echo _o( 'Accept', 'bookly', 'button' ) ?></span>
	    </button>
	 </div><?php
    endif;
	
else:

?><div class="ab-row-fluid ab-nav-steps ab-clear"><?php 
	if ('rejected' != $appointment_status):
?><button class="ab-left ab-cancel-appointment ab-btn ladda-button" data-style="zoom-in" style="margin-right: 10px;" data-spinner-size="40">
        <span class="ladda-label"><?php echo _o( 'Cancel Appointment', 'bookly', 'button' ) ?></span>
    </button><?php endif;
    if ('accepted' == $appointment_status): 
    ?><button class="ab-right ab-to-fifth-step ab-btn ladda-button" data-style="zoom-in" data-spinner-size="40">
        <span class="ladda-label"><?php echo _o( 'Next', 'bookly', 'button' ) ?></span>
    </button><?php endif;
    if('rejected' == $appointment_status):
    ?><button class="ab-right ab-start-over ab-btn ladda-button" data-style="zoom-in" data-spinner-size="40">
        <span class="ladda-label"><?php echo _o( 'Start Over', 'bookly', 'button' ) ?></span>
    </button><?php endif;
?></div><?php

endif;