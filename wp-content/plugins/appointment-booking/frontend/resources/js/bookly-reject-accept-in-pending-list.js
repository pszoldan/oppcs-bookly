(function($){
	$(document).ready(function(){
		$("a.ab-btn-reject-appointment, a.ab-btn-accept-appointment").each(function(){
			$(this).one('click', update_appointment_status);
		});
		
		function update_appointment_status(e){
			e.preventDefault();
			var button = this;
            var ladda = Ladda.create(button);
            ladda.start();
            var new_status = $(button).hasClass('ab-btn-reject-appointment') ? 'rejected' : 'accepted';
			$.ajax({
			    url         : $(button).attr('data-ajax'),
			    data        : { action: 'ab_update_appointment_status', 
			    					token: $(button).attr('data-token'),
			    					new_status: new_status },
			    dataType    : 'json',
			    xhrFields   : { withCredentials: true },
			    crossDomain : 'withCredentials' in new XMLHttpRequest(),
			    success     : function (response) {
			    		console.log(response); // TODO delete debug line
			            if (!response.success) {
			                ladda.stop();
			            	$("div#entry-content").html(response.html);
			            }else{
			            	$(button).closest("tr").removeClass("ab-appointment-status-pending")
			            		.addClass("ab-appointment-status-" + new_status );
			            	$(button).closest("td").html( new_status.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) );
			            	ladda.stop();
			           }
			        }
			    });                        	
			}
	});
})(jQuery)