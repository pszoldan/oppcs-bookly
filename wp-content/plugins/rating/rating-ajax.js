jQuery( document ).ready( function( $ ) {
	var $rating_form = $( "div.rating_envelope form.rating" );
	var $rating_star = $rating_form.find( "input[name='rating']" );
	var $rating_button = $rating_form.find( "button.submit_rating" );
	$rating_star.on( "change", function() {
		if ( 0 < $rating_star.filter( ":checked" ).length ) {
			$rating_button.attr( "disabled", false );
		} else {
			$rating_button.attr( "disabled", true );
		}
	} );
	$rating_button.on( "click", function( e ) {
		e.preventDefault();
		e.stopImmediatePropagation();
		var data = $rating_form.serialize() + "&action=register_rating";
		$.post( ajax_object.ajax_url, data, function( response ) {
			$( "div.rating_envelope" ).html( "<p>" + response + "</p>" );
		});
	});
});