<?php
/**
 * Plugin Name: Rating
 * Plugin URI: http://easyitme.com
 * Description: Adds shortcode for lightweight rating system
 * Version: 1.0.0
 * Changelog: 1.0.0 created
 * Author: Peter Szoldan
 * Author URI: http://easyitme.com
 * License: strict
 */

define( "RATING_RESOURCE_VERSION", "1.0" );

register_activation_hook( __FILE__, 'rating_install');
register_deactivation_hook( __FILE__, 'rating_uninstall');

global $rating_table_name, $wpdb;
$rating_table_name = $wpdb->prefix . 'ratings';

add_action( 'wp_enqueue_scripts', 'rating_enqueue_scripts' );
function rating_enqueue_scripts() {
	wp_register_style('rating', plugins_url( 'rating.css', __FILE__), array(), RATING_RESOURCE_VERSION);
	wp_enqueue_style("rating");
}

function rating_append_to_option( $o, $msg ) {
	$v = get_option( $o ) || '';
	if ( '' == $v ) {
		add_option( $msg );
	} else {
		update_option( $msg );
	}
}
function rating_admin_notice(){
	$msg = get_option( "rating_admin_notice" );
	if ( $msg ){ echo $msg; }
	delete_option( "rating_admin_notice" );
}
add_action( 'admin_notices', 'rating_admin_notice' );

function ratingPrintWpdbErrorAsAdminNotice($act){
	global $wpdb;
	$errStr = $wpdb->use_mysqli ? mysqli_error( $wpdb->dbh) : mysql_error( $wpdb->dbh );
	if ($errStr) {
		ob_start();
		$wpdb->show_errors();
		$wpdb->print_error();
		$wpdb->hide_errors();
		$err = ob_get_contents();
		ob_end_clean();
	}else{
		$err = false;
	}

	echo $err ? preg_replace('<div id="error">', "<div class=\"error\"><p>$act</p>", $err)
	:  "<div class=\"updated\"><p>$act</p></div>";
}
function rating_install(){
	global $wpdb, $rating_table_name;
	$charset_collate = $wpdb->get_charset_collate();
	ob_start(NULL, 0, PHP_OUTPUT_HANDLER_STDFLAGS);

	// please note key definitions MUST come after all column definitions because of the
	// picky nature of the dbDelta() funcion
	// see more here: https://codex.wordpress.org/Creating_Tables_with_Plugins
	$sql = "CREATE TABLE " . $rating_table_name . " (
	id BIGINT NOT NULL AUTO_INCREMENT,
	UID BIGINT UNSIGNED NOT NULL,
	timestamp TIMESTAMP DEFAULT NOW(),
	user_agent VARCHAR(1024),
	platform VARCHAR(255),
	browser VARCHAR(255),
	majorversion INT(8),
	minorversion INT(8),
	client_ip VARCHAR(16),
	user_id INT(10) UNSIGNED NOT NULL,
	rating INT(1) UNSIGNED,
	answer_1 VARCHAR(255),
	answer_2 VARCHAR(255),
	answer_3 VARCHAR(255),
	PRIMARY KEY  ( id ),
	KEY UID ( UID ),
	KEY user_id ( user_id ),
	KEY platform ( platform ),
	KEY browser ( browser ),
	KEY majorversion ( majorversion ),
	KEY minorversion ( minorversion )
	) $charset_collate";
	//dlprintWpdbErrorAsAdminNotice("Creating '$debug_logger_table_name' table if it not exists.");
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	$update_result = dbDelta( $sql );
	ratingPrintWpdbErrorAsAdminNotice( '' );
// 	if ( is_wp_error( $result ) ) {

// 		$result = implode( '<br />', $result->$get_error_messages() );
// 	}
	echo "<div class='notice notice-info is-dismissible'><p>"
		.implode( '<br />', $update_result )."</p></div>";

	debug_logger_new_entry( array(
			'error_level' => 10, // info
			'plugin' => 'Rating Admin',
			'comment' => 'Rating plugin enabled',
	));
	rating_append_to_option( 'rating_admin_notice', ob_get_clean() );
}
function rating_uninstall(){
	debug_logger_new_entry( array(
			'error_level' => 10, // info
			'plugin' => 'Rating Admin',
			'comment' => 'Rating plugin disabled',
	));
}

// shortcode
add_shortcode( 'rating', 'rating' );
function rating( $atts ) {
	global $form_option_name;
	$attributes = shortcode_atts( array( 'uid' => 0 ), $atts );
	
	$output = "<div class=\"rating_envelope\">"
		."<div class=\"rating_placeholder\"><p>"
		.__('Please click here to rate the video quality.', 'Rating')
		."</p></div>";
	$output .= "<div class=\"rating_form\">";
	$output .= "<form class=\"rating\" method=\"post\">";
	$output .= star_rating();
	$output .= "<input name=\"UID\" type=\"hidden\" value=\"".$attributes[ 'uid' ]."\" />";
	$output .= "<fieldset>" . get_option( $form_option_name ) . "</fieldset>"; // extra questions
	$output .= "<button class=\"submit_rating\" disabled>" . __( "Submit rating", "Rating" ) . "</button>";
	$output .= "</form></div></div>";
	return $output;
}

// ajax handling of rating posted
add_action( 'wp_enqueue_scripts', 'populate_ajax_object' );
function populate_ajax_object(){
	wp_enqueue_script( 'rating-ajax', plugins_url( '/rating-ajax.js', __FILE__ ), array('jquery') );
	wp_localize_script( 'rating-ajax', 'ajax_object',
			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_ajax_register_rating', 'register_rating' );
function register_rating() {
	global $wpdb, $rating_table_name;
	if ( 0 < get_current_user_id() ) {
		$form_params = array( 'UID', 'rating', 'answer_1', 'answer_2', 'answer_3' );
		$params = array();
		foreach ( $form_params as $k ){
			$params[ $k ] = $_POST[ $k ];
		}
		$form_params[ 'UID' ] *= 1;
		$params[ 'user_agent' ] = $_SERVER['HTTP_USER_AGENT'];
		$browser_cap = get_browser(null, true);
		$params[ 'platform' ] = $browser_cap[ 'platform' ];
		$params[ 'browser' ] = $browser_cap[ 'browser' ];
		$v = explode( ".", $browser_cap[ 'version' ] );
		$params[ 'majorversion' ] = $v[ 0 ] * 1;
		$params[ 'minorversion' ] = $v[ 1 ] * 1;
		$params[ 'client_ip' ] = $_SERVER["REMOTE_ADDR"];
		$params[ 'user_id' ] = get_current_user_id();
		
		$wpdb->field_types[ 'UID' ] = '%d';
		$wpdb->field_types[ 'majorversion' ] = '%d';
		$wpdb->field_types[ 'minorversion' ] = '%d';
		$wpdb->field_types[ 'user_id' ] = '%d';
		$wpdb->field_types[ 'rating' ] = '%d';
		$sql = $wpdb->insert( $rating_table_name, $params );
		
		echo __( "Thank you for your feedback!", "Rating" );
		wp_die();
	} else {
		wp_die();
	}
}

// admin settings
$form_option_name = 'rating_plugin_questions_form';
define("RATING_SETTINGS_PAGE_NAME", "rating_plugin");
add_action('admin_menu', 'rating_plugin_admin_add_page');
function rating_plugin_admin_add_page() {
	add_options_page('Rating', 'Rating Settings', 'manage_options',
			RATING_SETTINGS_PAGE_NAME, 'rating_plugin_options_page');
}
function rating_plugin_add_settings_link($links){
	$l = '<a href="options-general.php?page='.RATING_SETTINGS_PAGE_NAME
	.'">' . ( 'Settings' ) . '</a>';
	array_push( $links, $l);
	return $links;
}
add_filter( "plugin_action_links_" . plugin_basename(__FILE__), 'rating_plugin_add_settings_link');
$option_group = 'rating_plugin_options';
function rating_plugin_options_page(){
	global $option_group;
	echo "<div>";
	echo "<h2>Rating Settings</h2>";
	echo "<form action=\"options.php\" method=\"post\">";
	settings_fields( $option_group );
	do_settings_sections('rating_plugin');
	echo "<p class=\"submit\"><input name=\"submit\" type=\"submit\" value=\"Save Changes\" /></p>";
	echo "</form>";
	echo "</div>";
}
add_action('admin_init', 'rating_plugin_admin_init');
function rating_plugin_admin_init(){
	global $option_group, $form_option_name;

	register_setting( $option_group, $form_option_name, '' );
	add_settings_section('form_options', 'Email Report Options', 'form_options_text', 'rating_plugin');
	add_settings_field( $form_option_name,
			'Please design the questions to go with the rating form here as html. Make sure the names '
			.'of the inputs are answer_1, answer_2, and answer_3.',
			'form_input', 'rating_plugin', 'form_options');
}
function form_options_text(){
	echo '<p><i>Set options related rating questions.</i></p>';
}
function form_input(){
	global $form_option_name;
	$opt = get_option( $form_option_name );
	echo "<strong>&lt;fieldset&gt;</strong><br/ >"
		."<textarea id='$form_option_name' name='$form_option_name' placeholder='Enter HTML here...' "
		." rows='10' cols='90'>{$opt}</textarea>"
		."<br /><strong>&lt;/fieldset&gt;</strong>";
}
function star_rating() {
	$output = <<<EOF
  <fieldset class="stars">
  <label class="stars">
    <input type="radio" name="rating" value="1" />
    <span class="icon">★</span>
  </label>
  <label class="stars">
    <input type="radio" name="rating" value="2" />
    <span class="icon">★</span>
    <span class="icon">★</span>
  </label>
  <label class="stars">
    <input type="radio" name="rating" value="3" />
    <span class="icon">★</span>
    <span class="icon">★</span>
    <span class="icon">★</span>   
  </label>
  <label class="stars">
    <input type="radio" name="rating" value="4" />
    <span class="icon">★</span>
    <span class="icon">★</span>
    <span class="icon">★</span>
    <span class="icon">★</span>
  </label>
  <label class="stars">
    <input type="radio" name="rating" value="5" />
    <span class="icon">★</span>
    <span class="icon">★</span>
    <span class="icon">★</span>
    <span class="icon">★</span>
    <span class="icon">★</span>
  </label>
  </fieldset>
EOF;
	$output.= ">>>" . PHP_INT_MAX ."<<<";
	return $output;
}