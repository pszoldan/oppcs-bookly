function frmShowEntry(l, n, p, v, w, y) {
    var r = jQuery("div#left-area a[id^=\"frm_edit_\"]")
      , q = r.html()
      , x = jQuery("div#left-area div[id^=\"" + n + "\"]")
      , C = x.html();
    if ( 0 < x.has("form").length ) { // we're editing currently
    	if (!confirm("You have not saved your edits yet on the current note. If you load another note, your edits will be lost.")){return;}
    	x.parent().find("span:has(a[href^=\"javascript:frmOPPCSCancelEdit\"])").remove(); // rip out cancel button
    }
    x.html('<span class="frm-loading-img" id="' + n + l + '_img"/>');
    var pageId = p;
    r.remove();
    jQuery.ajax({
        type: "POST",
        url: frm_js.ajax_url,
        dataType: "html",
        data: {
            action: "frm_show_entry",
            post_id: p,
            entry_id: l,
            id: v,
            nonce: frm_js.nonce
        },
        success: function(C) {
        	var res = jQuery('response_data', C)[0].firstChild.textContent;
        	resNoCdata = res.replace(/^\[CDATA\[/, '').replace(/\]\]$/, '');
        	resDecoded = resNoCdata.replace(/&amp;/g,'&').replace(/&quot;/g,'"').replace(/&#039;/g,"'").replace(/&lt;/g,'<').replace(/&gt;/g,'>');
        	x.replaceWith(resDecoded);
        	r = jQuery("div#left-area a#frm_edit_" + l);
        	r.attr("href", 
        			r.attr("href").replace(/(frmOPPCSEditEntry\([^,]*,[^,]*,)([^,]*)(,[^,]*,[^,]*,[^,]*\))/, "$1"+pageId+"$3")
        	);
        	updateShownNoteIcon();
        	jQuery('html, body').animate({
	        		scrollTop: jQuery("div.um-profile-nav").offset().top
	        			- jQuery("#main-header").height()
	        			- jQuery("#top-header").height(),
        		}, 500,"swing",
	    		function(){ 
					setTimeout(function(){ 
		    			jQuery('html, body').animate({
		    				scrollTop: jQuery("div.um-profile-nav").offset().top
		    					- jQuery("#main-header").height()
		    					- jQuery("#top-header").height()
						}, 100);
					}, 20);
				}
        	);
        	
        }
    })
}

function frmOPPCSEditEntry(entry_id,prefix,post_id,form_id,cancel,hclass){
	var $edit = jQuery(document.getElementById('frm_edit_'+entry_id));
	var label = $edit.html();
	var $cont = jQuery(document.getElementById(prefix+entry_id));
	var orig = $cont.html();
	$cont.html('<span class="frm-loading-img" id="'+prefix+entry_id+'"></span><div class="frm_orig_content" style="display:none">'+orig+'</div>');
	jQuery.ajax({
		type:'POST',url:frm_js.ajax_url,dataType:'html',
		data:{action:'frm_entries_edit_entry_ajax', post_id:post_id, entry_id:entry_id, id:form_id, nonce:frm_js.nonce},
		success:function(html){
			$cont.children('.frm-loading-img').replaceWith(html);
			$edit.replaceWith('<span id="frm_edit_'+entry_id+'"><a href="javascript:frmOPPCSCancelEdit('
					+entry_id+',\''+prefix+'\',\''+ frmFrontForm.escapeHtml(label) +'\','+post_id+','+form_id+',\''
					+hclass+'\')" class="'+hclass+'">'+cancel+'</a></span>');
			jQuery("input[name=\"frm_action\"]").val("create"); // set to create mode
			jQuery("input[name=\"id\"]").remove(); // delete old id
			jQuery(window).on("beforeunload", function(e){
				return "The edited client note has not been saved yet. If you navigate away, your edits will be lost.";
			});
			// http://stackoverflow.com/questions/2360655/jquery-event-handlers-always-execute-in-order-they-were-bound-any-way-around-t/2641047#2641047
			//[name] is the name of the event "click", "mouseover", .. 
			//same as you'd pass it to bind()
			//[fn] is the handler function
			(function($){
				$.fn.bindFirst = function(name, fn) {
						// bind as you normally would
						// don't want to miss out on any jQuery magic
						this.on(name, fn);
						 // Thanks to a comment by @Martin, adding support for
						 // namespaced events too.
						return this.each(function() {
							var handlers = $._data(this, 'events')[name.split('.')[0]];
							// take out the handler we just inserted from the end
							var handler = handlers.pop();
							// move it at the beginning
							handlers.splice(0, 0, handler);
				 		});
					}
				;
			}(jQuery));
			jQuery("#frm_form_14_container .frm-show-form").bindFirst("submit",function(){
				jQuery(window).off("beforeunload"); // don't do the warning when we're legitimately updating
			});
		}
	});
}
function frmOPPCSCancelEdit(l, n, p, v, w, y){
	jQuery(window).off("beforeunload");
    var r = jQuery(document.getElementById("frm_edit_" + l))
		, q = r.find("a")
		, x = q.html();
	q.hasClass("frm_ajax_edited") || (q = jQuery(document.getElementById(n + l)),
	q.children(".frm_forms").replaceWith(""),
	q.children(".frm_orig_content").fadeIn("slow").removeClass("frm_orig_content"));
	r.replaceWith('<a id="frm_edit_' + l + '" class="frm_edit_link ' + y + '" href="javascript:frmOPPCSEditEntry(' 
			+ l + ",'" + n + "'," + v + "," + w + ",'" + frmFrontForm.escapeHtml(x) + "','" + y + "')\">" + p + "</a>")
}
function updateShownNoteIcon(){
	var shownId = jQuery("div#left-area a[id^=\"frm_edit_\"]").attr("id").match(/\d+$/)[0] * 1;
	var shownIcon = jQuery("table.oppcs_sessions_and_notes a.frm_edit_link#frm_edit_" + shownId);
	shownIcon.addClass("currently_shown");
	shownIcon.closest("tr").addClass("currently_shown");
	var notShownIcons = jQuery("table.oppcs_sessions_and_notes a.frm_edit_link:not(\"#frm_edit_" + shownId + "\")");
	notShownIcons.removeClass("currently_shown");
	notShownIcons.closest("tr").removeClass("currently_shown");
}
var maxRunsForPositionSessionAndNotesWidget = 30;
var RunsForPositionSessionAndNotesWidget = 0;
function positionSessionsAndNotesWidget(){
	if ("none" == jQuery("div#sidebar").css("float")){ return; } // don't do anyhing if sidebar doesn't float but comes after content (narrow screens, like mobile)
	var $nav = jQuery("div.um-profile-nav");
	var $widget = jQuery("div.textwidget:has(table.oppcs_sessions_and_notes)");
	if(!(0 == $nav.length || 0 == $widget.length)){
		var navTop = $nav.offset()['top'];
		var widgetOffsetTop = $widget.offset()['top'];
		var widgetTop = parseInt('0' + $widget.css("top"));
		$widget.css("top",navTop - widgetOffsetTop + widgetTop).css("position","relative");
	}
	if( maxRunsForPositionSessionAndNotesWidget > RunsForPositionSessionAndNotesWidget++ ){
		setTimeout(positionSessionsAndNotesWidget, 167);
	}
}
positionSessionsAndNotesWidget();
jQuery(document).ready(function(){
	updateShownNoteIcon();
	RunsForPositionSessionAndNotesWidget = 0;
	positionSessionsAndNotesWidget();
});