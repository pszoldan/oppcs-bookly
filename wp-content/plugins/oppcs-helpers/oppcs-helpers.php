<?php

/**
 * Plugin Name: OPPCS Helpers
 * Plugin URI: http://easyitme.com
 * Description: Tracks provider-client relationships, updates profile pages, generates video room if there is an active appointment. Depends on: Bookly, Formidable Pro.
 * Version: 1.0.0
 * Author: Peter Szoldan
 * Author URI: http://easyitme.com
 * License: strict
 */
global $oppcs_helpers_db_version;
$oppcs_helpers_db_version= '1.0.0';

global $oppcs_provider_client_link_table_name, $wpdb,
	$oppcs_provider_client_link_table_name, $oppcs_appointments_table,
	$oppcs_customer_appointments_table, $oppcs_customers_table,
	$oppcs_staff_table;
$oppcs_provider_client_link_table_name = $wpdb->prefix . 'oppcs_provider_client_links';
$oppcs_appointments_table = $wpdb->prefix . 'ab_appointments';
$oppcs_customer_appointments_table = $wpdb->prefix . 'ab_customer_appointments';
$oppcs_customers_table = $wpdb->prefix . 'ab_customers';
$oppcs_staff_table = $wpdb->prefix . 'ab_staff';

foreach(scandir(__DIR__) as $f){
	$spl = new SplFileInfo(__DIR__ . '/' . $f);
	if ($spl->isFile() && 'php' == $spl->getExtension()){
		require_once($spl->getPathname());
	}
}
register_activation_hook( __FILE__, 'oppcs_helpers_install');
register_deactivation_hook( __FILE__, 'oppcs_helpers_uninstall');
function printWpdbErrorAsAdminNotice($act){
	global $wpdb;
	$errStr = $wpdb->use_mysqli ? mysqli_error( $wpdb->dbh) : mysql_error( $wpdb->dbh );
	if ($errStr) {
		ob_start();
		$wpdb->show_errors();
		$wpdb->print_error();
		$wpdb->hide_errors();
		$err = ob_get_contents();
		ob_end_clean();
	}else{
		$err = false;
	}

	echo $err ? preg_replace('<div id="error">', "<div class=\"error\"><p>$act</p>", $err)
					:  "<div class=\"updated\"><p>$act</p></div>";
}
function oppcs_admin_notice(){ // class = updated | error | update-nag
	$msg = get_option("oppcs_installation_message");
	if($msg){	echo $msg; }
	delete_option("oppcs_installation_message");
}
add_action('admin_notices', 'oppcs_admin_notice');
function oppcs_helpers_install(){
	global $wpdb, $oppcs_helpers_db_version, $oppcs_provider_client_link_table_name,
		$oppcs_provider_client_link_table_name, $oppcs_appointments_table,
		$oppcs_customer_appointments_table, $oppcs_customers_table,
		$oppcs_staff_table;
	global $oppcs_admin_notices;
	$charset_collate = $wpdb->get_charset_collate();

	ob_start(NULL, 0, PHP_OUTPUT_HANDLER_STDFLAGS);
	
	$wpdb->hide_errors();

	$sql = "CREATE TABLE $oppcs_provider_client_link_table_name (
	provider_id bigint(20) unsigned NOT NULL,
	client_id int(10) unsigned NOT NULL,
	PRIMARY KEY  (provider_id,client_id)
	) $charset_collate";
	printWpdbErrorAsAdminNotice("Creating table $oppcs_provider_client_link_table_name.");
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	// add trigger to create provider client links
	$sql = "DROP TRIGGER IF EXISTS `OppcsProvCliLink`";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Dropping trigger if exists `OppcsProvCliLink`.");
	
	$sql = "CREATE TRIGGER `OppcsProvCliLink` AFTER INSERT ON `$oppcs_customer_appointments_table`
					FOR EACH ROW BEGIN
						SET @staff_id = (SELECT `staff_id` FROM `$oppcs_appointments_table` WHERE `id` = NEW.appointment_id);
						SET @staff_wp_id = (SELECT `wp_user_id` FROM `$oppcs_staff_table` WHERE `id` = @staff_id);
						SET @customer_wp_id = (SELECT `wp_user_id` FROM `$oppcs_customers_table` WHERE `id` = NEW.customer_id);
						INSERT IGNORE INTO `$oppcs_provider_client_link_table_name`
						VALUES (@staff_wp_id, @customer_wp_id);
					END";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Creating trigger `OppcsProvCliLink` to store provider - client links.");
	
	// generate links for existing appointments
	$sql = "INSERT IGNORE INTO `$oppcs_provider_client_link_table_name`
					SELECT `staff`.`wp_user_id` AS `provider_id`, `customer`.`wp_user_id` AS `client_id` FROM `$oppcs_appointments_table` AS `t1`
					JOIN `$oppcs_customer_appointments_table` AS `t2` ON `t1`.`id` = `t2`.`appointment_id`
					JOIN `$oppcs_staff_table` AS `staff` ON `staff`.`id` = `t1`.`staff_id`
					JOIN `$oppcs_customers_table` AS `customer` ON `customer`.`id` = `t2`.`customer_id`
	";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Initializing provider - client links from existing appointments.");
	
	$uid_generator = "146564756945 + FLOOR( 831646658765 * RAND() )";
	// UID column, generation trigger to Appointments+ plugin's main table
	$sql = "ALTER TABLE `$oppcs_appointments_table` ADD `UID` bigint(20) unsigned NOT NULL";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Adding UID to appointments table (`$oppcs_appointments_table`). "
			."A 'duplicate column' error is normal if the plugin used to be activated before.");
	
	// generate UIDs where it doesn't exist
	$sql = "UPDATE `$oppcs_appointments_table` SET `UID` = $uid_generator WHERE `UID` = 0";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Generating UIDs for existing appointments.");
	
	// create unique index to preclude duplicate UIDs
	$sql = "CREATE UNIQUE INDEX `UID` ON $oppcs_appointments_table (`UID`)";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Adding UID index to appointments table (`$oppcs_appointments_table`). "
				."A 'duplicate key' error is normal if the plugin used to be activated before.");
	
	$sql = "DROP TRIGGER IF EXISTS `OppcsGenerateUID`";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Dropping trigger if exists `OppcsGenerateUID`.");
		
	$sql= "CREATE TRIGGER `OppcsGenerateUID` BEFORE INSERT ON `$oppcs_appointments_table`
	FOR EACH ROW BEGIN
	SET NEW.UID = $uid_generator;
	END";
	$wpdb->query($sql);
	printWpdbErrorAsAdminNotice("Creating trigger `OppcsGenerateUID` to generate UIDs for new appointments.");
	
	add_option( 'oppcs_helpers_db_version', $oppcs_helpers_db_version );

	// set default settings values
	add_option('video_available_before_appointment_start_minutes', 15);

	$oppcs_admin_notices = ob_get_contents();
	ob_end_clean();
	add_option('oppcs_installation_message', $oppcs_admin_notices);
}
function oppcs_helpers_uninstall(){
	global $wpdb, $oppcs_helpers_db_version, $oppcs_provider_client_link_table_name;
	$wpdb->show_errors();

	$wpdb->query("DROP TRIGGER IF EXISTS `OppcsProvCliLink`");
	$wpdb->query("DROP TABLE $oppcs_provider_client_link_table_name");
}

add_action('init', 'load_translations');
function load_translations(){
	load_plugin_textdomain( 'OPPCS', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
// 	global $l10n;
// 	echo"<pre>".print_r($l10n, true)."</pre>";exit();
}

// admin settings
define("SETTINGS_PAGE_NAME", "oppcs_plugin");
add_action('admin_menu', 'plugin_admin_add_page');
function plugin_admin_add_page() {
	add_options_page('OPPCS Helper', 'OPPCS Helper Settings', 'manage_options', SETTINGS_PAGE_NAME, 'oppcs_plugin_options_page');
}
function oppcs_plugin_add_settings_link($links){
	$l = '<a href="options-general.php?page='.SETTINGS_PAGE_NAME.'">' . ( 'Settings' ) . '</a>';
	array_push( $links, $l);
	return $links;
}
add_filter( "plugin_action_links_" . plugin_basename(__FILE__), 'oppcs_plugin_add_settings_link');
function oppcs_plugin_options_page(){
	echo "<div>";
	echo "<h2>OPPCS Helper Settings</h2>";
	echo "<form action=\"options.php\" method=\"post\">";
	settings_fields('oppcs_plugin_options');
	do_settings_sections('oppcs_plugin');
	echo "<p class=\"submit\"><input name=\"submit\" type=\"submit\" value=\"Save Changes\" /></p>";
	echo "</form>";
	echo "</div>";
}

add_action('admin_init', 'oppcs_plugin_admin_init');
function oppcs_plugin_admin_init(){
	// soon minutes setting
	register_setting('oppcs_plugin_options', 'video_available_before_appointment_start_minutes', 'oppcs_number_validate');
	add_settings_section('video_options', 'Video Options', 'video_options_text', 'oppcs_plugin');
	add_settings_field('video_available_before_appointment_start_minutes',
		'Video room is available this many minutes before the meeting starts, enter a value between 0 and 1440',
		'soon_minutes_input', 'oppcs_plugin', 'video_options');

	// after minutes setting
	register_setting('oppcs_plugin_options', 'video_available_after_appointment_end_minutes', 'oppcs_number_validate');
	add_settings_section('video_options', 'Video Options', 'video_options_text', 'oppcs_plugin');
	add_settings_field('video_available_after_appointment_end_minutes',
	'Video room is available this many minutes after the meeting ends, enter a value between 0 and 1440',
	'oppcs_after_minutes_input', 'oppcs_plugin', 'video_options');
	
	// shortcode pattern settings
	register_setting('oppcs_plugin_options', 'video_shortcode_pattern');
	add_settings_section('video_options', 'Video Options', 'video_options_text', 'oppcs_plugin');
	add_settings_field('video_shortcode_pattern',
		'Shortcode pattern to use to generate the video shortcode. '
		.'You may include %ID% and %TITLE%, '
		.'which will be filled with the corresponding parameters. '
		.'This pattern helps the OPPCS plugin to cooperate with third-party webrtc plugins '
		.'without releasing the OPPCS plugin again.',
		'oppcs_shortcode_pattern_input', 'oppcs_plugin', 'video_options');

	// formidable pro settings
	register_setting('oppcs_plugin_options', 'oppcs_formidable_pro_form_id', 'oppcs_number_validate');
	add_settings_section('formidable_options', 'Formidable Pro Client Records Options', 'formidable_options_text', 'oppcs_plugin');
	add_settings_field('oppcs_formidable_pro_form_id',
		'Client records are kept using the Formidable Pro form with this id',
		'oppcs_formidable_pro_form_id_input', 'oppcs_plugin', 'formidable_options');
	
	register_setting('oppcs_plugin_options', 'oppcs_note_attach_minutes', 'oppcs_number_validate');
	add_settings_section('formidable_options', 'Formidable Pro Client Records Options', 'formidable_options_text', 'oppcs_plugin');
	add_settings_field('oppcs_note_attach_minutes',
		'Client records are shown on the same row with the session if they are created between '
		.'this many minues before the start of the session and this many minues after the end of the session',
		'oppcs_note_attach_minutes_input', 'oppcs_plugin', 'formidable_options');
	
}
function video_options_text(){
	echo "<p>Settings related to video meetings.</p>";
}
function formidable_options_text(){
	echo "<p>Settings related to Formidable Pro client record management.</p>";
}
function soon_minutes_input(){
	$opt = get_option('video_available_before_appointment_start_minutes');
	echo "<input id='video_available_before_appointment_start_minutes' name='video_available_before_appointment_start_minutes' "
			." type='number' min='0' max='1440' value='{$opt}' />";
}
function oppcs_after_minutes_input(){
	$opt = get_option('video_available_after_appointment_end_minutes');
	echo "<input id='video_available_after_appointment_end_minutes' name='video_available_after_appointment_end_minutes' "
			." type='number' min='0' max='1440' value='{$opt}' />";
}
function oppcs_formidable_pro_form_id_input(){
	$opt = get_option('oppcs_formidable_pro_form_id');
	echo "<input id='oppcs_formidable_pro_form_id_input' name='oppcs_formidable_pro_form_id' "
			." type='number' min='1' value='{$opt}' />";
}
function oppcs_note_attach_minutes_input(){
	$opt = get_option('oppcs_note_attach_minutes');
	echo "<input id='oppcs_note_attach_minutes_input' name='oppcs_note_attach_minutes' "
			." type='number' min='0' value='{$opt}' />";
}
function oppcs_shortcode_pattern_input(){
	$opt = get_option('video_shortcode_pattern');
	echo "<textarea id='video_shortcode_pattern' name='video_shortcode_pattern' "
			." rows=\"4\" cols=\"80\" "
			."placeholder='Example: [twilio_video_feed id=\"%ID%\" title=\"%TITLE%\"]'>"
			.$opt."</textarea>";
}
function oppcs_number_validate($input){ return 1 * $input; }
