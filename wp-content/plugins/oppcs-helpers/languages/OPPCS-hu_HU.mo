��          �      �       H     I  u   U     �     �  
   �     �  �     �   �  �   �  B  �  j  �  )   L  �   v          2	  �   B	     �	     �	     
  2   
  �   J
     =  �   ^  �  M  �  �  )   �  �   �                           
                     	                 Date & time If you experience problems with the video or audio, please refresh this page in your browser, with Ctrl+R or Ctrl+F5. Notes Past Sessions And Notes Session ID Video chat room with %s, id: %s We have detected that you are using Google Chrome but couldn't determine the version.
If you have problems with the video chat, please make sure to upgrade your Chrome to
the latest version. We have detected that you are using Google Chrome version %d.
This version is known to have a bug which causes it to show a black screen
instead of your chat partner's video feed. Please upgrade to the latest
version of Chrome, to at least version 45. We have detected that you are using Google Chrome version %d.
This version is too old and does not support this video chat technology.
Please upgrade to the latest version of Chrome, to at least version 45. We have detected that you are using Google Chrome version 36.
This particular version is known to have a bug which causes it to show a black screen
instead of your chat partner's video feed, even though video chat has been supported
since version 32. Please upgrade to the latest version of Chrome, to at least version 45. We have detected that you are using a browser other than Google Chrome. 
Unfortunately, as of late 2015, the only browser reliably implementing 
this video chat technology is Chrome, version at least 45.
We expect that in a few months all other major browsers will follow along, 
but right now our recommendation is to switch to Chrome, to a version at least 45. You don't have any upcoming appointments. Your next appointment is at %s. You will be able to log in to the video 
channel %d minutes before that. Please refresh or revisit this page at that time. Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Project-Id-Version: WPML_EXPORT
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: EasyITMe
Language: hu_HU
MIME-Version: 1.0
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.6
 Dátum és idő Ha problémát tapasztal a videóval vagy a hanggal, akkor próbálja meg frissíteni ezt az oldalt a browserben, a Ctrl+R vagy Ctrl+F5 billentyűkombinációval. Jegyzet Beszélgetések és jegyzetek Azonosító Videó beszélgetés, partner: %s, azonosító: %s Észleltük, hogy Ön a Chrome böngészőt használja, de nem tudtuk meghatározni a verziószámot.
Ha problémái vannak a videó beszélgetéssel, akkor kérjük, hogy győződjön meg róla, hogy a 
legutolsó Chrome verziót használja. Észleltük, hogy az Ön által használt Google Chrome veriója %d.
Ez a verzió sajnos egy olyan hibát tartalmaz, ami miatt a videó nem működik, mindig
csak fekete képet mutat a partnere képe helyett. Kérjük, frissítsen a Chrome legújabb,
de legalábbis a 45-ös verziójára. Észleltük, hogy Ön a Google Chrome-ot használja, verzió száma %d.
Sajnos, ez a verzió túl régi, és nem támogatja a videó beszélgetéseket.
Kérjük, frissítsen a Google Chrome legújabb, de legalábbis a 45-ös verziójára. Észleltük, hogy Ön a Google Chrome 36-os verzióját használja.
Ez a verzió sajnos egy olyan hibát tartalmaz, ami miatt a videó nem működik, mindig
csak fekete képet mutat a partnere képe helyett, annak ellenére, hogy a Chrome hivatalosan
a Chrome 32-es verziója óta támogatja a videó beszélgetést. Kérjük, frissítsen a Chrome legújabb,
de legalábbis a 45-ös verziójára. Észleltük, hogy Ön nem a Chrome böngészőt használja. 
Sajnos, jelenleg, 2015 vége felé, a Chrome az egyetlen böngésző, 
és az is csak a 45-ös verziótól kezdve, amiben a videó beszélgetés 
technológia megbízhatóan működik. Várhatóan a többi browser is eljut 
ide a következő hónapokban, de egyelőre azt javasoljuk, hogy 
a legjobb eredmény érdekében váltson Chrome böngészőre, 
legalább a 45-ös verzióra. Önnek nincs előjegyzés a naptárában. Az Ön következő időpontja %s. Az előtt az időpont előtt %d perccel fog majd tudni
bejelentkezni a videó beszélgetésbe ezen az oldalon. Kérjük, akkor frissítse ezt az oldalt,
vagy látogasson el ide újra. 