<?php
add_shortcode('oppcs_bookly_calendar', 'oppcs_bookly_calendar');

function oppcs_bookly_calendar($atts)
{
    $controller = new AB_CalendarController();

    $ajax_url = admin_url('admin-ajax.php');
    print <<<EOF
    <script>
        window.ajaxurl = "$ajax_url";
    </script>
EOF;
    $controller->index();
}
