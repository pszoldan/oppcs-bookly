<?php

// getting rid of references to Google Calendar from the staff profile
add_action( "wp_ajax_ab_edit_staff", "ab_edit_staff_output_capture", 1);
function ab_edit_staff_output_capture(){
	ob_start('ab_edit_staff_override_buffer_handler');
}
function ab_edit_staff_override_buffer_handler($buffer, $phase){
	$divOpen = "\s*<div[^>]*>\s*";
	$divClose = "\s*<\/div>\s*";
	$h4Open = "\s*<h4[^>]*>\s*";
	$h4Close = "\s*<\/h4>\s*";
	$divComplete = "{$divOpen}[\s\S]*?{$divClose}";
	$formGroupWithGoogleCalendar = 
	"(<div\sclass=\"form-group\">.*?Google\sCalendar.*?<\/div>)\s*<div\sclass=\"form-group\">";
	$newBuffer = preg_replace(
			 "/{$divOpen}{$divOpen}{$h4Open}Google\sCalendar\sintegration{$h4Close}"
			."{$divComplete}"
			."{$divClose}{$divClose}"
			."{$divOpen}{$divComplete}{$divClose}"
		."/", "", $buffer);
	$newBuffer = preg_replace(
			"/{$divOpen}{$divOpen}{$h4Open}Google\sNaptár\sintegrációja{$h4Close}" 
			."{$divComplete}"
	."{$divClose}{$divClose}"
	."{$divOpen}{$divComplete}{$divClose}"
	."/", "", $newBuffer);
	return $newBuffer;
}
