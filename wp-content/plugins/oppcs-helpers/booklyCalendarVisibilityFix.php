<?php

function deregister_filter_bookly($a){
	if("bookly" == $a){return true;}
	return !!preg_match( "/^ab-/", $a);
}

add_action( 'wp_enqueue_scripts', 'oppcs_bookly_calendar_visibility_css_fix',20000); // at the end
function oppcs_bookly_calendar_visibility_css_fix(){
	$page = get_post();
	if ( $page instanceof  WP_Post ) {
		$pageId = $page->ID;
	// 	echo"<pre>".print_r($page, true)."</pre><pre>$pageId</pre>";exit();
		if	( in_array( $pageId, array( 847 )) // dequeue for appointment page
				//  "make-an-appointment-3", "idopont-foglalas" have id 847 
			){
			wp_dequeue_style('um_minified');
		}
		if ( in_array( $pageId, array( 829 ))){ // private client registration
			wp_deregister_script( 'bookly' );
			global $wp_scripts, $wp_styles;
			$deregs = array_filter( $wp_scripts->queue, "deregister_filter_bookly" );
			foreach($deregs as $script){
				wp_dequeue_script( $script );
				wp_deregister_script( $script );
			}
			$dereg_styles = array_filter( $wp_styles->queue, "deregister_filter_bookly" );
			foreach($dereg_styles as $style){
				wp_dequeue_style( $style );
				wp_deregister_style( $style );
			}
		}
	}
}