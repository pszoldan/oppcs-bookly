<?php
add_action( 'wp_enqueue_scripts', 'divi_oppcs_enqueue_styles' );

function divi_oppcs_enqueue_styles() {
	wp_enqueue_style( 'divi-style', get_template_directory_uri() . '/style.css' );
}