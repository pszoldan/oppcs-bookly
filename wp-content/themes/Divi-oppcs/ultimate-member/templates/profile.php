<div class="um <?php echo $this->get_class( $mode ); ?> um-<?php echo $form_id; ?>">
        <div class="um-form">
<?php
do_action('um_profile_before_header', $args ); 
if ( um_is_on_edit_profile() ) { echo '<form method="post" action="">'; }
do_action('um_profile_header_cover_area', $args );
do_action('um_profile_header', $args );
function is_provider_of_client(){
	global $wpdb, $oppcs_provider_client_link_table_name;
	$req_user = um_get_requested_user();
	if (!$req_user){$req_user = get_current_user_id();} // if req user empty, it's himself
	$query = "
			SELECT COUNT(*) FROM `$oppcs_provider_client_link_table_name`
			WHERE `provider_id`=".get_current_user_id()." AND `client_id`=$req_user";
	$res = $wpdb->get_var( $query );
	return !!$res;
}
{ // navbar
	ob_start();
	do_action('um_profile_navbar', $args );
	$navbar = ob_get_clean();
	// rip pencil icon if not applicable
	if (!is_provider_of_client()){
		$navbar = preg_replace("/<div[^>]*class=[^>]*um-profile-nav-posts[\s\S]*?<\/div>/", "", $navbar);
	}
	echo $navbar;
}
$nav = $ultimatemember->profile->active_tab;
$subnav = ( get_query_var('subnav') ) ? get_query_var('subnav') : 'default';
print "<div class='um-profile-body $nav $nav-$subnav'>";
$FORM_ID = get_option('oppcs_formidable_pro_form_id');
$umReqestedUserId = um_get_requested_user();
um_fetch_user( get_current_user_id() );
$umRole = um_user('role'); 
um_fetch_user( um_get_requested_user() );
$umRequestedRole = um_user('role');
if (			"posts" == $nav && "on-line-service-providers" == $umRole
		&&	!um_is_user_himself()){
	global $wpdb, $frmdb;
	$sql = "SELECT `t1`.`id`, `t1`.`created_at` FROM `{$frmdb->entries}` AS `t1` JOIN `{$frmdb->entry_metas}` AS `t2` ON `t1`.`id`=`t2`.`item_id`
		WHERE `user_id`=" . get_current_user_id() . " AND `form_id`=$FORM_ID AND `field_id`=151"
		." AND `meta_value` = ". um_get_requested_user()
		." ORDER BY `created_at` DESC LIMIT 1"; // get the latest one
	$entry_id = $wpdb->get_col( $sql );
	if( $entry_id && is_array($entry_id) && array_key_exists( 0, $entry_id ) ){
		print "<div id='form_edit_area_{$entry_id[0]}'>";
		echo FrmEntriesController::show_entry_shortcode( array( 'id' => $entry_id[0], 'include_blank' => 1) );
		print '</div>';
		$editLinkOriginal = FrmProEntriesController::entry_edit_link( array( 'id' => $entry_id[0], 'label' => 'Edit',
				'prefix' => 'form_edit_area_' ));
		$editLinkOPPCS = str_replace( "frmEditEntry", "frmOPPCSEditEntry", $editLinkOriginal );
		echo $editLinkOPPCS;
	}else{
		echo FrmFormsController::get_form_shortcode( array( 'id' => $FORM_ID, 'title' => false, 'description' => false,
		'client_id' => um_get_requested_user() ) );
	}
}
else{ // if not 'posts' subnav, just call normal UM display
        // Custom hook to display tabbed content
        ob_start();
		do_action("um_profile_content_{$nav}", $args);
		do_action("um_profile_content_{$nav}_{$subnav}", $args);
		$profile = ob_get_clean();
		// rip out services field for providers
		if ( "main" == $nav && um_is_user_himself() ) {
			$dom = new DOMDocument;
			$dom->loadHTML( mb_convert_encoding($profile, 'HTML-ENTITIES', 'UTF-8') );
			$xpath = new DOMXPath( $dom );

				$divs = $xpath->query(".//div[@data-key='services']");
				foreach($divs as $div){
					if (!um_is_on_edit_profile() ){ // just delete if not edited
						$div->parentNode->removeChild( $div );
					} else { // hide if edited, so when the form is saved, it still sends data - fyi, this field is required
						$div->setAttribute("style", "display:none;");
					}
				}
				$profile=preg_replace(array("/^\<\!DOCTYPE.*?<html><body>/si",
		                                    "!</body></html>$!si"),
		                              "",$dom->saveHTML());
		} else if ( "main" == $nav && !um_is_user_himself() && "on-line-service-providers" == $umRequestedRole ) {
			// get provider's services
			global $wpdb;
			$umReqestedUserId;
			$sql = "SELECT `se`.`title`, `ss`.`price` FROM `".$wpdb->prefix."ab_staff_services` AS `ss`
					JOIN `".$wpdb->prefix."ab_services` AS `se` ON `se`.`id`=`ss`.`service_id`
					JOIN `".$wpdb->prefix."ab_staff` AS `st` ON `st`.`id`=`ss`.`staff_id`
					WHERE `wp_user_id`=$umReqestedUserId";
			$services = $wpdb->get_results( $sql );
			
			// replace original list with one from bookly as that it includes price as well
			$dom = new DOMDocument;
			$dom->loadHTML( mb_convert_encoding($profile, 'HTML-ENTITIES', 'UTF-8') );
			$xpath = new DOMXPath( $dom );
			// create table for services list
			$table = $dom->createElement( 'table' );
			$tbody = $dom->createElement( 'tbody' );
			$table->appendChild( $tbody );
			foreach ( $services as $service ) {
				$tr = $dom->createElement( 'tr' );
				$td = $dom->createElement( 'td' );
				$td->nodeValue = __( $service->title, "bookly" );
				$tr->appendChild( $td );
				$td2 = $dom->createElement( 'td' );
				$td2->nodeValue = number_format( $service->price, 2, ",", "&nbsp;" );
				$style = $dom->createAttribute( 'style' );
				$style->value = "text-align: right;";
				$td2->appendChild( $style );
				$tr->appendChild( $td2 );
				$tbody->appendChild( $tr );
			}
			
			$services_field = $xpath->query(".//div[@data-key='services']/div[@class='um-field-area']/div[@class='um-field-value']");
			foreach ($services_field as $sf ) {
				$sf->parentNode->replaceChild( $table, $sf );
			}
			$profile=preg_replace(array("/^\<\!DOCTYPE.*?<html><body>/si",
	                                    "!</body></html>$!si"),
	                              "",$dom->saveHTML());
		}
		
 		echo $profile;
}
                        ?>

                <?php if ( um_is_on_edit_profile() ) { ?></form><?php } ?>

        </div>

</div>
